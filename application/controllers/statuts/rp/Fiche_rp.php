<?php

use Entities\Adresse;
use Entities\Statut_judiciaire;

class Fiche_rp extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->context->setStatut('judiciaire');
	}

	public function index() {
		$sinistre = $this->context->getSinistre();

		$this->twig->display('sinistre/rp.html.twig', array('topbar2' => 1));
	}

}
