<?php

class Mails_rp extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->context->setStatut('rp');
		$this->load->helper('mailer');
	}

	public function index() {
		$this->declaration();
	}

	public function declaration() {

		$user = $this->context->getUser();
		$sinistre = $this->context->getSinistre();
		$dest = getMailDest(21);
		$type = 21;
		$patron = 'rp/declaration_asqua.html.twig';

		if (!isset($_POST['objet'])) {
			$data_preview = array();
			$data_preview['sinistre'] = $sinistre;
			$data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$preview = getMailPreview($patron, $data_preview);
			$dest['destinataires'] = str_replace(",", ", ", $dest['destinataires']);
			$dest['CC'] = str_replace(",", ", ", $dest['CC']);
			$data = array();
			$data['pj'] = true;
			$data['mail_content'] = $preview;
			$data['title'] = "Déclaration ASQUA";
			$data = array_merge($data, $dest);
			$data['active'] = 2;
			$data['topbar2'] = 2;

			$alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => $type));
			if ($alreadysent)
				$data['alreadysent'] = $alreadysent;

			$this->twig->display('mails/rp.html.twig', $data);
		} else {
			if ($sinistre->getStatut_initial() != 1) {
				$token_asqua = uniqid();
				$sinistre->setToken_asqua($token_asqua);
				$this->doctrine->em->persist($sinistre);
				$this->doctrine->em->flush();
				$_POST['token_asqua'] = $token_asqua;
			}
			$objet = $_POST['objet'];
			$to = $dest['destinataires'];
			$cc = $dest['CC'];
			$from = $user->email;
			$_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$files = null;
			if (isset($_POST['docs']) && $_POST["docs"] != "")
				$files = splitDocs($_POST['docs']);
			$_POST['sinistre'] = $sinistre;
			$mail = makeMail($patron, $_POST);
			try {
				sendMail($mail, $objet, $to, $from, $cc, $files);
				saveMail($sinistre->getRp(), "Déclaration ASQUA Référé préventif", $mail, $objet, $to, $from, $cc, $files, $type);
			} catch (Exception $e) {
				echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
				redirect('statuts/rp/mails_rp/declaration');
				return;
			}
			echo '<script>alert("Email envoyé")</script>';
			redirect('statuts/rp/mails_rp/declaration');
			return;
		}
	}

	public function avocat() {

		$user = $this->context->getUser();
		$sinistre = $this->context->getSinistre();

		if (!$sinistre->is_synchro()) {
			alert("Le sinistre doit être synchronisé avec Asqua pour obtenir l'adresse de l'avocat");
			redirect(last_url());
		}
		if (!$sinistre->getJudiciaire()->getAvocat_dossier()) {
			alert("L'adresse de l'avocat n'a pas été renseignée par Asqua");
			redirect(last_url());
		}
		$dest = getMailDest(11);
		$patron = 'judiciaire/mail_avocat.html.twig';
		$dest['destinataires'] = $sinistre->getJudiciaire()->getAvocat_dossier()->getEmail();

		if (!isset($_POST['objet'])) {
			$data_preview = array();
			$data_preview['sinistre'] = $sinistre;
			$data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$preview = getMailPreview($patron, $data_preview);
			$data = array();
			$data['mail_content'] = $preview;
			$data['title'] = "Mail Avocat";
			$data = array_merge($data, $dest);
			$data['active'] = 3;
			$data['topbar2'] = 2;
			$alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 11));
			if ($alreadysent)
				$data['alreadysent'] = $alreadysent;

			$this->twig->display('mails/judiciaire.html.twig', $data);
		} else {
			if ($sinistre->getStatut_initial() != 1) {
				$token_asqua = uniqid();
				$sinistre->setToken_asqua($token_asqua);
				$this->doctrine->em->persist($sinistre);
				$this->doctrine->em->flush();
				$_POST['token_asqua'] = $token_asqua;
			}
			$objet = $_POST['objet'];
			$to = $dest['destinataires'];
			$cc = $dest['CC'];
			$from = $user->email;
			$_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$files = null;
			if (isset($_POST['docs']))
				$files = splitDocs($_POST['docs']);
			$_POST['sinistre'] = $sinistre;
			$mail = makeMail($patron, $_POST);
			try {
				sendMail($mail, $objet, $to, $from, $cc, $files);
				saveMail($sinistre->getJudiciaire(), "Mail Avocat", $mail, $objet, $to, $from, $cc, $files, 11);
			} catch (Exception $e) {
				echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
				redirect('statuts/judiciaire/mails_jud/declaration');
				return;
			}
			echo '<script>alert("Email envoyé")</script>';
			redirect('statuts/judiciaire/mails_jud/declaration');
			return;
		}
	}

	public function docs2avocat() {

		$user = $this->context->getUser();
		$sinistre = $this->context->getSinistre();

		if (!$sinistre->is_synchro()) {
			alert("Le sinistre doit être synchronisé avec Asqua pour obtenir l'adresse de l'avocat");
			redirect(last_url());
		}
		if (!$sinistre->getJudiciaire()->getAvocat_dossier()) {
			alert("L'adresse de l'avocat n'a pas été renseignée par Asqua");
			redirect(last_url());
		}

		if ($user->role == 5 || $user->role == 10) {
			$dest = getMailDest(12);
			$type = 12;
		}
		if ($user->role == 2) {
			$dest = getMailDest(13);
			$type = 13;
		}
		$patron = 'judiciaire/docs2avocat.html.twig';
		$dest['destinataires'] = $sinistre->getJudiciaire()->getAvocat_dossier()->getEmail();
		if (!isset($dest['CC']))
			$dest['CC'] = "";
		$dest['CC'] .= "," . $sinistre->getJudiciaire()->getExpert_compagnie_judiciaire()->getEmail();

		if (!isset($_POST['objet'])) {
			$data_preview = array();
			$data_preview['sinistre'] = $sinistre;
			$data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$preview = getMailPreview($patron, $data_preview);
			$dest['destinataires'] = str_replace(",", ", ", $dest['destinataires']);
			$dest['CC'] = str_replace(",", ", ", $dest['CC']);
			$data = array();
			$data['mail_content'] = $preview;
			$data['title'] = "Transmission documents Avocat";
			$data = array_merge($data, $dest);
			$data['active'] = 4;
			$data['topbar2'] = 2;
			$data['pj'] = true;
			$alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => $type));
			if ($alreadysent)
				$data['alreadysent'] = $alreadysent;

			$this->twig->display('mails/judiciaire.html.twig', $data);
		} else {
			if ($sinistre->getStatut_initial() != 1) {
				$token_asqua = uniqid();
				$sinistre->setToken_asqua($token_asqua);
				$this->doctrine->em->persist($sinistre);
				$this->doctrine->em->flush();
				$_POST['token_asqua'] = $token_asqua;
			}
			$objet = $_POST['objet'];
			$to = $dest['destinataires'];
			$cc = $dest['CC'];
			$from = $user->email;
			$_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$files = null;
			if (isset($_POST['docs'])) {
				$files = splitDocs($_POST['docs']);
				$_POST['docs'] = $files;
			}
			$_POST['sinistre'] = $sinistre;
			$mail = makeMail($patron, $_POST);
			try {
				sendMail($mail, $objet, $to, $from, $cc, $files);
				saveMail($sinistre->getJudiciaire(), "Transmission documents Avocat", $mail, $objet, $to, $from, $cc, $files, $type);
			} catch (Exception $e) {
				echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
				redirect('statuts/judiciiare/mails_jud/declaration');
				return;
			}
			echo '<script>alert("Email envoyé")</script>';
			redirect('statuts/judiciaire/mails_jud/declaration');
			return;
		}
	}

	public function docs2rcr() {

		$user = $this->context->getUser();
		$sinistre = $this->context->getSinistre();
		$dest = getMailDest(14);
		$patron = 'judiciaire/docs2rcr.html.twig';


		if (!isset($_POST['objet'])) {
			$data_preview = array();
			$data_preview['sinistre'] = $sinistre;
			$data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$preview = getMailPreview($patron, $data_preview);

			$dest['destinataires'] = str_replace(",", ", ", $dest['destinataires']);
			$dest['CC'] = str_replace(",", ", ", $dest['CC']);
			$data = array();
			$data['mail_content'] = $preview;
			$data['title'] = "Transmission documents rcr";
			$data = array_merge($data, $dest);

			$data['pj'] = true;

			$alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 14));
			if ($alreadysent)
				$data['alreadysent'] = $alreadysent;

			$data['active'] = 5;
			$data['topbar2'] = 2;
			$this->twig->display('mails/judiciaire.html.twig', $data);
		} else {
			$objet = $_POST['objet'];
			$to = $dest['destinataires'];
			$cc = $dest['CC'];
			$from = $user->email;
			$files = null;
			$_POST['sinistre'] = $sinistre;
			$_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
			$i = 1;
			$files = array();
			if (isset($_POST['docs'])) {
				$docs = splitDocs($_POST['docs']);
				$_POST['docs'] = $docs;
			}
			$mail = makeMail($patron, $_POST);

			try {
				sendMail($mail, $objet, $to, $from, $cc, $files);
				saveMail($sinistre->getJudiciaire(), "Transmission documents rcr", $mail, $objet, $to, $from, $cc, $files, 14);
			} catch (Exception $e) {
				echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
				redirect('statuts/judiciaire/mails_jud/declaration');
				return;
			}
			echo '<script>alert("Email envoyé")</script>';
			redirect('statuts/judiciaire/mails_jud/declaration');
			return;
		}
	}

}
