<?php

class Interne_rp extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->statut = "rp";
        $this->context->setStatut('Rp');
    }

    public function index() {
        $this->memo();
    }

    public function memo() {
        $this->title = "Memos";
        $sinistre = $this->context->getSinistre();
        $statut = $sinistre->getRp();
        $user = $this->session->userdata('user');
        $user = $user->prenom . " " . $user->nom;

        if (isset($_POST['memo'])) {
            $memo = new Entities\Memo();
            $memo->setDate(new DateTime());
            $memo->setAuteur($user);
            $memo->setMemo($_POST['memo']);

            $statut->addMemo($memo);

            $this->doctrine->em->persist($statut);
            $this->doctrine->em->persist($memo);
            $this->doctrine->em->flush();
        }

        $memos = $statut->getMemos();
        $this->twig->display('interne/rp/memo.html.twig', array('memos' => $memos, 'user' => $user, 'topbar2' => 5, 'active' => 1));
    }

}
