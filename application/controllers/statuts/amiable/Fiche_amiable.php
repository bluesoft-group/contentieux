<?php

use Entities\Statut_AV1;
use Entities\Adresse;
use Entities\Cabinet_expert;
use Entities\Expert;
use Entities\Statut_amiable;

class Fiche_amiable extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut('amiable');
    }

    public function index() {
        $sinistre = $this->context->getSinistre();

        if (!isset($_POST['expert_cie_nom_contact'])) {
            $this->twig->display('sinistre/amiable.html.twig', array('topbar2' => 1));
            return;
        }

        $amiable = $sinistre->getAmiable();
        if (!$amiable instanceof Statut_amiable)
            $amiable = new Statut_amiable();

        $amiable = $this->mapToEntity($_POST, $amiable);

        $sinistre->setAmiable($amiable);
        $this->doctrine->em->persist($amiable);
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();


        $this->twig->display('sinistre/amiable.html.twig', array('topbar2' => 1));
    }

    public function mapToEntity($post, Statut_amiable $amiable) {

        //$amiable->setRef_dossier_expert_compagnie($post['ref_dossier_expert_cie']);

        /*
         * Cabinet expert compagnie
         */

        $cabinet_expert_cie = $amiable->getCabinet_expert_compagnie();

        if (!$cabinet_expert_cie instanceof \Entities\Contact) {
            $cabinet_expert_cie = new \Entities\Contact();
        }

        $cabinet_expert_cie->setNom($post['expert_cie_nom_contact']);
        $cabinet_expert_cie->setTelephone($post['expert_cie_telephone_contact']);
        $cabinet_expert_cie->setEmail($post['expert_cie_email_contact']);

        $adresse_cab = $cabinet_expert_cie->getAdresse();

        if (!$adresse_cab instanceof Adresse) {
            $adresse_cab = new Adresse;
        }

        $adresse_cab->setRue1($post['expert_cie_rue1']);
        $adresse_cab->setRue2($post['expert_cie_rue2']);
        $adresse_cab->setRue3($post['expert_cie_rue3']);
        $adresse_cab->setCp($post['expert_cie_code_postal']);
        $adresse_cab->setCommune($post['expert_cie_commune']);
        $cabinet_expert_cie->setAdresse($adresse_cab);

        $amiable->setCabinet_expert_compagnie($cabinet_expert_cie);

        /*
         * Contact expert
         */

        $expert = $amiable->getExpert_compagnie();

        if (!$expert instanceof \Entities\Contact) {
            $expert = new \Entities\Contact;
        }

        $expert->setNom($post['expert_nom_contact']);
        $expert->setPrenom($post['expert_prenom_contact']);
        $expert->setTelephone($post['expert_telephone_contact']);
        $expert->setEmail($post['expert_email_contact']);

        $amiable->setExpert_compagnie($expert);

        $amiable->setRef_dossier_expert_compagnie($post['ref_expert_cie']);

        //locateurs d'ouvrage

        $i = 1;
        $los = $amiable->getLo();
        while (isset($post["lo" . $i . "_nom_contact"])) {
            $lo = $los->get($i - 1);
            if (!$lo instanceof Entities\Contact) {
                $lo = new Entities\Contact();
                $adresse = new Adresse();
                $lo->setAmiable($amiable);
            } else {
                $adresse = $lo->getAdresse();
            }

            $lo->setNom($post["lo" . $i . "_nom_contact"]);
            $lo->setEmail($post["lo" . $i . "_email_contact"]);
            $lo->setTelephone($post["lo" . $i . "_telephone_contact"]);

            $adresse->setRue1($post["lo" . $i . "_rue1"]);
            $adresse->setRue2($post["lo" . $i . "_rue2"]);
            $adresse->setRue3($post["lo" . $i . "_rue3"]);
            $adresse->setCp($post["lo" . $i . "_code_postal"]);
            $adresse->setCommune($post["lo" . $i . "_commune"]);

            $lo->setAdresse($adresse);

            if ($lo->getId() == null) {
                $amiable->addLo($lo);
            }
            $i++;
        }

        $lo = $los->get($i - 1);
        while ($lo instanceof Entities\Contact) {
            $this->doctrine->em->remove($lo);
            $this->doctrine->em->flush();
            $i++;
            $lo = $los->get($i - 1);
        }

        return $amiable;
    }

}
