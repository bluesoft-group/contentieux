<?php

class Interne_do extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->statut = "do";
        $this->context->setStatut('DO');
    }

    public function index() {
        $this->memo();
    }

    public function memo() {
        $this->title = "Memos";
        $sinistre = $this->context->getSinistre();
        $statut = $sinistre->getDo();
        $user = $this->session->userdata('user');
        $user = $user->prenom . " " . $user->nom;

        if (isset($_POST['memo'])) {
            $memo = new Entities\Memo();
            $memo->setDate(new DateTime());
            $memo->setAuteur($user);
            $memo->setMemo($_POST['memo']);

            $statut->addMemo($memo);

            $this->doctrine->em->persist($statut);
            $this->doctrine->em->persist($memo);
            $this->doctrine->em->flush();
        }

        $memos = $statut->getMemos();
        $this->twig->display('interne/do/memo.html.twig', array('memos' => $memos, 'user' => $user, 'topbar2' => 5, 'active' => 1));
    }

    public function pathologie() {
        $this->twig->display('interne/fiche_pathologie.html.twig', array(
            'topbar_2' => 'topbar/topbar2do.html.twig',
            'topbar_3' => 'interne/do/topbar_interne.html.twig',
            'topbar2' => 5,
            'active' => 2
        ));
    }

    public function mails() {
        $affaire = $this->context->getAffaire();
        $id_g = $affaire->getId_g_affaire();
        
        $this->load->model('ImportMails');
        $mails = $this->ImportMails->get($id_g);
        
        $this->twig->display('interne/mails_recus.html.twig', array(
            'topbar_2' => 'topbar/topbar2do.html.twig',
            'topbar_3' => 'interne/do/topbar_interne.html.twig',
            'topbar2' => 5,
            'active' => 3,
            'mails' => $mails
        ));
    }

}
