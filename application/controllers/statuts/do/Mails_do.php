<?php

//ini_set("display_errors", 1);
//error_reporting(E_ALL);

class Mails_do extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut('DO');
        $this->load->helper('Mailer');
    }

    public function index() {
        $this->declaration();
    }

    public function declaration() {

        $user = $this->context->getUser();
        $dest = getMailDest(1);
        $patron = 'do/declaration_asqua.html.twig';
        $sinistre = $this->context->getSinistre();

        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);

            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Declaration ASQUA";
            $data = array_merge($data, $dest);

            $data['pj'] = true;
            $data['active'] = 2;
            $data['topbar2'] = 2;

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 1));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;
            $this->twig->display('mails/do.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            $docs = explode(",", $_POST['docs']);
            foreach ($docs as $doc) {
                $files[] = getFile($doc);
            }
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $token_asqua = uniqid();
            $_POST['token_asqua'] = $token_asqua;
            $sinistre->setToken_asqua($token_asqua);
            $this->doctrine->em->persist($sinistre);
            $this->doctrine->em->flush();
            $mail = makeMail($patron, $_POST);
            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getDo(), "Déclaration ASQUA DO", $mail, $objet, $to, $from, $cc, $files, 1);
                if (ENVIRONMENT == "demo") {
                    $this->load->library('asqua_lib');
                    $this->asqua_lib->synchro_demo($sinistre);
                }
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/do/mails_do/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';

            redirect('statuts/do/mails_do/declaration');
            return;
        }
    }

    public function indisponibilite() {

//                ini_set("display_errors", 1);
//        error_reporting(E_ALL);

        $user = $this->context->getUser();
        $dest = getMailDest(2);
        $patron = 'do/indisponibilite_reunion.html.twig';
        $sinistre = $this->context->getSinistre();
        if (!@$sinistre->getDo()->getContact_expert()->getEmail() && !@$sinistre->getDo()->getCabinet_expert()->getEmail()) {
            alert("L'adresse email de l'expert doit être renseignée");
            redirect(last_url());
        }
        $mail_expert = $sinistre->getDo()->getContact_expert()->getEmail();
        if (!$mail_expert)
            $mail_expert = $sinistre->getDo()->getCabinet_expert()->getEmail();
        $dest['destinataires'] = $mail_expert;


        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Indisponibilité réunion DO";
            $data = array_merge($data, $dest);

            $data['pj'] = true;

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 2));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['active'] = 3;
            $data['topbar2'] = 2;
            $this->twig->display('mails/do.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $mail_expert = $sinistre->getDo()->getContact_expert()->getEmail();
            if (!$mail_expert)
                $mail_expert = $sinistre->getDo()->getCabinet_expert()->getEmail();
            $dest['destinataires'] = $mail_expert;
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $mail = makeMail($patron, $_POST);
            try {
                $files = array();
                $docs = explode(",", $_POST['docs']);
                foreach ($docs as $doc_id) {
                    $doc = getFile($doc_id);
                    $files[] = $doc;
                }
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getDo(), "Indisponibilité réunion DO", $mail, $objet, $to, $from, $cc, $files, 2);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/do/mails_do/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/do/mails_do/declaration');
            return;
        }
    }

    public function indisponibilite2() {


        $user = $this->context->getUser();
        $dest = getMailDest(3);
        $patron = 'do/indisponibilite_reunion2.html.twig';
        $sinistre = $this->context->getSinistre();

        if (!@$sinistre->getDo()->getContact_expert()->getEmail() && !@$sinistre->getDo()->getCabinet_expert()->getEmail()) {
            alert("L'adresse email de l'expert doit être renseignée");
            redirect(last_url());
        }

        $mail_expert = $sinistre->getDo()->getContact_expert()->getEmail();
        if (!$mail_expert)
            $mail_expert = $sinistre->getDo()->getCabinet_expert()->getEmail();
        $dest['destinataires'] = $mail_expert;

        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);

            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Indisponibilité réunion & non implication de QC";
            $data = array_merge($data, $dest);
            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 3));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['active'] = 5;
            $data['topbar2'] = 2;
            $this->twig->display('mails/do.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $mail = makeMail($patron, $_POST);
            $files = null;
            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getDo(), "Indisponibilité réunion DO & non implication de QC", $mail, $objet, $to, $from, $cc, $files, 3);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/do/mails_do/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/do/mails_do/declaration');
            return;
        }
    }

    public function grilleAnalyse() {

        $user = $this->context->getUser();
        $dest = getMailDest(4);
        $patron = 'do/grille_analyse_sinistre.html.twig';
        $sinistre = $this->context->getSinistre();

        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Grille d'analyse sinistre";
            $data = array_merge($data, $dest);

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 4));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['active'] = 4;
            $data['topbar2'] = 2;
            $this->twig->display('mails/do.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $mail = makeMail($patron, $_POST);
            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getDo(), "Grille d'analyse sinistre", $mail, $objet, $to, $from, $cc, $files, 4);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/do/mails_do/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/do/mails_do/declaration');
            return;
        }
    }

    public function disponibilite() {

        $user = $this->context->getUser();
        $dest = getMailDest(18);
        $sinistre = $this->context->getSinistre();

        if (!@$sinistre->getDo()->getContact_expert()->getEmail() && !@$sinistre->getDo()->getCabinet_expert()->getEmail()) {
            alert("L'adresse email de l'expert doit être renseignée");
            redirect(last_url());
        }

        $mail_expert = $sinistre->getDo()->getContact_expert()->getEmail();
        if (!$mail_expert)
            $mail_expert = $sinistre->getDo()->getCabinet_expert()->getEmail();
        $dest['destinataires'] = $mail_expert;
        $patron = 'do/disponibilite_reunion.html.twig';


        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Présence réunion DO";
            $data = array_merge($data, $dest);

            $data['pj'] = true;

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 18));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['active'] = 6;
            $data['topbar2'] = 2;
            $this->twig->display('mails/do.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $dest['destinataires'] = $sinistre->getDo()->getContact_expert()->getEmail();
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $mail = makeMail($patron, $_POST);
            try {
                $files = array();
                $docs = explode(",", $_POST['docs']);
                foreach ($docs as $doc_id) {
                    $doc = getFile($doc_id);
                    $files[] = $doc;
                }
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getDo(), "Présence réunion DO", $mail, $objet, $to, $from, $cc, $files, 18);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/do/mails_do/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/do/mails_do/declaration');
            return;
        }
    }

}
