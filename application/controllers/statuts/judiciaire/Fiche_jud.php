<?php

use Entities\Adresse;
use Entities\Statut_judiciaire;

class Fiche_jud extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut('judiciaire');
    }

    public function index() {
        $sinistre = $this->context->getSinistre();

        if (!isset($_POST['date_assignation'])) {
            $this->twig->display('sinistre/judiciaire.html.twig', array('topbar2' => 1));
            return;
        }

        $jud = $sinistre->getJudiciaire();
        if (!$jud instanceof Statut_judiciaire)
            $jud = new Statut_judiciaire;

        $jud = $this->mapToEntity($_POST, $jud);

        $sinistre->setJudiciaire($jud);

        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();


        $this->twig->display('sinistre/judiciaire.html.twig', array('topbar2' => 1));
    }

    public function mapToEntity($post, Statut_judiciaire $jud) {

        $jud->setRef_dossier_expert_compagnie($post['ref_dossier_expert_compagnie']);

        if ($post['date_assignation'] != "")
            $jud->setDate_assignation(date_create_from_format("Y-m-d", $post['date_assignation']));
        else
            $jud->setDate_assignation(null);

        /*
         * Cabinet expert cie jud
         */

        $cabinet_expert_cie = $jud->getCabinet_expert_compagnie_judiciaire();

        if (!$cabinet_expert_cie instanceof Entities\Contact)
            $cabinet_expert_cie = new Entities\Contact;

        $cabinet_expert_cie->setNom($post['compagnie_judiciaire_nom_contact']);
        $cabinet_expert_cie->setTelephone($post['compagnie_judiciaire_telephone_contact']);
        $cabinet_expert_cie->setEmail($post['compagnie_judiciaire_email_contact']);

        $adresse_cie = $cabinet_expert_cie->getAdresse();

        if (!$adresse_cie instanceof Adresse) {
            $adresse_cie = new Adresse;
        }
        $adresse_cie->setRue1($post['compagnie_judiciaire_rue1']);
        $adresse_cie->setRue2($post['compagnie_judiciaire_rue2']);
        $adresse_cie->setRue3($post['compagnie_judiciaire_rue3']);
        $adresse_cie->setCp($post['compagnie_judiciaire_code_postal']);
        $adresse_cie->setCommune($post['compagnie_judiciaire_commune']);

        $cabinet_expert_cie->setAdresse($adresse_cie);

        $jud->setCabinet_expert_compagnie_judiciaire($cabinet_expert_cie);

        $expert_cie = $jud->getExpert_compagnie_judiciaire();

        if (!$expert_cie instanceof Entities\Contact) {
            $expert_cie = new Entities\Contact;
        }

        $expert_cie->setNom($post['nom_expert_cie']);
        $expert_cie->setEmail($post['mail_expert_cie']);

        $jud->setExpert_compagnie_judiciaire($expert_cie);



        $cabinet_avocat_qc = $jud->getCabinet_avocat();

        if (!$cabinet_avocat_qc instanceof \Entities\Contact) {
            $cabinet_avocat_qc = new \Entities\Contact();
        }

        $cabinet_avocat_qc->setNom($post['avocat_qc_nom_contact']);
        $cabinet_avocat_qc->setTelephone($post['avocat_qc_telephone_contact']);
        $cabinet_avocat_qc->setEmail($post['avocat_qc_email_contact']);

        $jud->setCabinet_avocat($cabinet_avocat_qc);

        $adresse_cabinet = $cabinet_avocat_qc->getAdresse();
        if (!$adresse_cabinet instanceof Adresse) {
            $adresse_cabinet = new Adresse();
        }

        $adresse_cabinet->setRue1($post["avocat_qc_rue1"]);
        $adresse_cabinet->setRue2($post["avocat_qc_rue2"]);
        $adresse_cabinet->setRue3($post["avocat_qc_rue3"]);
        $adresse_cabinet->setCp($post["avocat_qc_code_postal"]);
        $adresse_cabinet->setCommune($post["avocat_qc_commune"]);

        $cabinet_avocat_qc->setAdresse($adresse_cabinet);

        // Avocat
        $avocat_dossier = $jud->getAvocat_dossier();

        if (!$avocat_dossier instanceof Entities\Contact) {
            $avocat_dossier = new Entities\Contact();
        }

        $avocat_dossier->setNom($post['nom_avocat']);
        $avocat_dossier->setPrenom($post['prenom_avocat']);
        $avocat_dossier->setEmail($post['mail_avocat']);

        $jud->setAvocat_dossier($avocat_dossier);

        // Collaborateur avocat
        $collaborateur_avocat_dossier = $jud->getCollaborateur_avocat_dossier();

        if (!$collaborateur_avocat_dossier instanceof Entities\Contact) {
            $collaborateur_avocat_dossier = new Entities\Contact();
        }

        $collaborateur_avocat_dossier->setNom($post['nom_collaborateur_avocat']);
        $collaborateur_avocat_dossier->setPrenom($post['prenom_collaborateur_avocat']);
        $collaborateur_avocat_dossier->setEmail($post['mail_collaborateur_avocat']);

        $jud->setCollaborateur_avocat_dossier($collaborateur_avocat_dossier);

        $jud->setRef_dossier_avocat($post["ref_dossier_avocat"]);


        return $jud;
    }

}
