<?php

class Interne_jud extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->statut = "judiciaire";
        $this->context->setStatut('Judiciaire');
    }
    
    public function index() {
        $this->memo();
    }

    public function memo() {
        $this->title = "Memos";
        $sinistre = $this->context->getSinistre();
        $statut = $sinistre->getJudiciaire();
        $user = $this->session->userdata('user');
        $user = $user->prenom . " " . $user->nom;

        if (isset($_POST['memo'])) {
            $memo = new Entities\Memo();
            $memo->setDate(new DateTime());
            $memo->setAuteur($user);
            $memo->setMemo($_POST['memo']);

            $statut->addMemo($memo);

            $this->doctrine->em->persist($statut);
            $this->doctrine->em->persist($memo);
            $this->doctrine->em->flush();
        }

        $memos = $statut->getMemos();
        $this->twig->display('interne/judiciaire/memo.html.twig', array('memos' => $memos, 'user' => $user, 'topbar2' => 5, 'active' => 1));
    }

}
