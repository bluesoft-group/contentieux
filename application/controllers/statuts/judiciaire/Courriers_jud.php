<?php

class Courriers_jud extends MY_Controller {

    public function index() {
        $this->editions();
    }

    public function editions() {
        $statut = $this->context->getSinistre()->getJudiciaire();
        $repo = $this->doctrine->em->getRepository('Entities\Edition');
        $editions = $repo->findBy(array('statut' => $statut), array('id' => 'DESC'));
        $user = $this->context->getUser();
        $this->twig->display('editions.html.twig', array('editions' => $editions, 'topbar_3' => 'courrier/judiciaire/topbar_courrier_jud.html.twig', 'active' => 2, 'topbar2' => 3, 'user' => $user));
    }

    private function validate_cr($statut) {

        $this->load->library('validator');

        $manquants = $this->validator->validate($statut, array(
            "sinistre.ref_asqua" => "Réf Asqua",
            "sinistre.affaire.libelle" => "Libelle affaire",
            "sinistre.affaire.code" => "Code affaire",
            "sinistre.affaire.missions" => "Missions affaire",
            "sinistre.adresse" => "Adresse sinistre",
            "sinistre.date_declaration" => "Date de declaration sinistre",
            "sinistre.date_reception_ouvrage" => "Date de reception de l'ouvrage",
            "expert_judiciaire.nom" => "Nom expert judiciaire",
        ));

        $expert_judiciaire = $statut->getExpert_judiciaire();
        if (!$expert_judiciaire->getPrenom() == 0) {
            $expert_judiciaire->setPrenom("");
            $this->doctrine->em->persist($expert_judiciaire);
            $this->doctrine->em->flush();
        }

        if (isset($manquants[0])) {
            return $manquants;
        }
        return false;
    }

    public function cr() {
        $sinistre = $this->context->getSinistre();
        $statut = $sinistre->getJudiciaire();

        $donnees_manquantes = $this->validate_cr($statut);


        $edition_repo = $this->doctrine->em->getRepository('Entities\Edition');
        $canedit = $edition_repo->canEdit($statut);
        $diffusion_defaut = "luc.moitry@qualiconsult.fr, laurent.ferhani@asqua.fr, fariza.chellah@qualiconsult.fr, contact@asqua.fr";
        $diffusion_defaut .= $statut->getExpert_compagnie_judiciaire()->getEmail();
        $rcr = $sinistre->getAffaire()->getRcr();
        if ($rcr == null) {
            $rcr = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $agence));
        } else {
            $rcr = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_rcr' => $rcr));
        }
        $diffusion_defaut .= ", " . $rcr->getMail_rcr();
        if ($rcr->getId_rcr() == 5785) {
            $diffusion_defaut .= ", thierry.samson@qualiconsult.fr";
        }
        
        if ($canedit == false) {
            echo '<script>alert("Un compte rendu est en attente de validation")</script>';
            redirect('/statuts/judiciaire/courriers_jud');
        }

        if (isset($_POST['redacteur'])) {
            $agence = $sinistre->getAffaire()->getId_agence();
            $_POST['diffusion_defaut'] = $diffusion_defaut;
            $date_reunion = date_create_from_format("Y-m-d", $_POST['date_reunion']);
            $nom_fichier = "CR reunion Judiciaire du " . $date_reunion->format('d/m/Y');
            $edition = $edition_repo->getSaved($statut);
            if (!$edition instanceof Entities\Edition)
                $edition = new \Entities\Edition();
            $type_e = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => 109));

            $user = $this->context->getUser();
            $username = $user->prenom . ' ' . $user->nom;
            $date = new DateTime();

            //Informations footer
            $this->load->model('Utils');
            $id_sv = $this->context->getAffaire()->getId_service();
            $footer = $this->Utils->getFooter($id_sv);
            $_POST = array_merge($_POST, $footer);

            $edition->setAuteur($username);
            $edition->setDate($date);
            $edition->setSinistre($sinistre);
            $edition->setStatut($statut);
            $edition->setEtat(0);
            $edition->setType_aspose(14);
            $edition->setNom_tmp($nom_fichier);
            $edition->setData(json_encode($_POST));
            if (isset($_POST['diffusion']))
                $diffusion = "," . $_POST['diffusion'];
            else
                $diffusion = "";
            $edition->setDiffusion($diffusion_defaut . $diffusion);

            $edition->setMail_auteur($user->email);

            $this->doctrine->em->persist($edition);
            $this->doctrine->em->flush();

            $this->load->library('AsposeLib');
            $this->asposelib->make_pdf(14, $_POST, $statut, $nom_fichier, $edition);

            $this->editions();
            return;
        }
        $edition = $edition_repo->getSaved($statut);
        if ($edition)
            $edition = json_decode($edition->getData());
        $this->twig->display('courrier/judiciaire/cr_judiciaire.html.twig', array('active' => 5, 'topbar2' => 3, 'edition' => $edition, 'diffusion' => $diffusion_defaut, 'manquants' => json_encode($donnees_manquantes)));
    }

    public function ct() {
        $sinistre = $this->context->getSinistre();
        $edition_repo = $this->doctrine->em->getRepository('Entities\Edition');
        $statut = $sinistre->getJudiciaire();
        $edition = $edition_repo->getSaved($statut, "courrier");

        if (isset($_POST['contact'])) {
            $this->load->library('AsposeLib');
            $type_e = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => 108));
            $categorie = $this->doctrine->em->getRepository('Entities\Categories_docs')->findOneBy(array('num' => 5));
            $nom_fichier = "Courrier du " . date('d-m-Y') . " à " . $_POST['contact'];

            $user = $this->context->getUser();
            $_POST['ca'] = $user->prenom . ' ' . $user->nom;
            $_POST['role'] = $user->role;
            $this->load->model('Utils');
            $_POST['ville_agence'] = $this->Utils->getVille($user->id_service);

            //Informations footer
            $this->load->model('Utils');
            $id_sv = $this->context->getAffaire()->getId_service();
            $footer = $this->Utils->getFooter($id_sv);
            $_POST = array_merge($_POST, $footer);



            if (!$edition instanceof Entities\Edition)
                $edition = new \Entities\Edition();

            $edition->setAuteur($_POST['ca']);
            $edition->setDate(new DateTime);
            $edition->setSinistre($sinistre);
            $edition->setStatut($statut);
            $edition->setEtat(0);
            $edition->setType_aspose(108);
            $edition->setNom_tmp($nom_fichier);
            $edition->setData(json_encode($_POST));
            $edition->setDiffusion(null);
            $edition->setMail_auteur($user->email);

            $this->doctrine->em->persist($edition);
            $this->doctrine->em->flush();


            $this->asposelib->make_pdf(108, $_POST, $statut, $nom_fichier, $edition, true);
            $this->editions();
            return;
        }
        if ($edition)
            $edition = json_decode($edition->getData());
        $this->twig->display('courrier/courrier_type.html.twig', array('topbar2' => 3, 'active' => 3, 'courrier' => $edition));
    }

    public function be() {
        $sinistre = $this->context->getSinistre();
        $edition_repo = $this->doctrine->em->getRepository('Entities\Edition');
        $statut = $sinistre->getJudiciaire();
        $edition = $edition_repo->getSaved($statut, "be");
        if (isset($_POST['contact'])) {
            $this->load->library('AsposeLib');
            $type_e = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => 107));
            $categorie = $this->doctrine->em->getRepository('Entities\Categories_docs')->findOneBy(array('num' => 5));
            $nom_fichier = "BE du " . date('d-m-Y') . " à " . $_POST['contact'];
            $user = $this->context->getUser();
            $_POST['ca'] = $user->prenom . ' ' . $user->nom;
            $_POST['role'] = $user->role;
            $this->load->model('Utils');
            $_POST['ville_agence'] = $this->Utils->getVille($user->id_service);

            //Informations footer
            $this->load->model('Utils');
            $id_sv = $this->context->getAffaire()->getId_service();
            $footer = $this->Utils->getFooter($id_sv);
            $_POST = array_merge($_POST, $footer);

            if (!$edition instanceof Entities\Edition)
                $edition = new \Entities\Edition();

            $edition->setAuteur($_POST['ca']);
            $edition->setDate(new DateTime);
            $edition->setSinistre($sinistre);
            $edition->setStatut($statut);
            $edition->setEtat(0);
            $edition->setType_aspose(107);
            $edition->setNom_tmp($nom_fichier);
            $edition->setData(json_encode($_POST));
            $edition->setDiffusion(null);
            $edition->setMail_auteur($user->email);

            $this->doctrine->em->persist($edition);
            $this->doctrine->em->flush();

            $this->asposelib->make_pdf(107, $_POST, $statut, $nom_fichier, $edition, true);
            $this->editions();
            return;
        }
        if ($edition)
            $edition = json_decode($edition->getData());
        $this->twig->display('courrier/bordereau_envoi.html.twig', array('active' => 2, 'topbar2' => 3, 'courrier' => $edition));
    }

}
