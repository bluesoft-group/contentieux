<?php

use Entities\Memo;

class Interne_c extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->menuIndex = 5;
        $this->context->setStatut('FS');
        $this->statut = "FS";
    }

    public function index() {
        $this->memo();
    }

    public function memo() {
        $this->title = "Memos du sinistre";
        $sinistre = $this->context->getSinistre();
        $id = $sinistre->getId();

        if (isset($_POST['memo'])) {
            $memo = new Entities\Memo();
            $memo->setDate(new DateTime());
            $auteur = $this->session->userdata('user');
            $auteur = $auteur->prenom . " " . $auteur->nom;
            $memo->setAuteur($auteur);
            $memo->setMemo($_POST['memo']);

            $sinistre->addMemo($memo);

            $this->doctrine->em->persist($memo);
            $this->doctrine->em->flush();
        }
        $memos = $this->doctrine->em->getRepository('Entities\Sinistre')->getMemos($id);

        $user = $this->session->userdata('user');
        $user = $user->prenom . " " . $user->nom;

        $this->display('interne/memo.html.twig', array('memos' => $memos, 'user' => $user));
    }

    public function deleteMemo($id) {
        $memoR = $this->doctrine->em->getRepository('Entities\Memo');
        $memo = $memoR->find($id);

        $user = $this->session->userdata('user');
        $ident = $user->prenom . " " . $user->nom;

        if ($memo->getAuteur() != $ident) {
            echo "<script>alert('Seul l\'auteur peut supprimer son memo')</script>";
            redirect('interne/memo_c');
            return;
        }

        $memo->delete();
        $this->doctrine->em->remove($memo);
        $this->doctrine->em->flush();
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

}
