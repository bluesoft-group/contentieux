<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sinistre_asqua
 *
 * @author martin.le-barbenchon
 */
class Sinistre_asqua {

    public $date_derniere_modif;
    public $base_asqua;
    public $statut;
    public $ref_asqua;
    public $ref_provisoire;
    public $libelle;
    public $description;
    public $etat;
    public $date_survenance;
    public $droc;
    public $type_garantie;
    public $judiciaire;
    public $expertise_do;
    public $assureur_do;
    public $prenom_expert_do;
    public $nom_expert_do;
    public $rue1_expert_do;
    public $rue2_expert_do;
    public $rue3_expert_do;
    public $cp_expert_do;
    public $commune_expert_do;
    public $tel_expert_do;
    public $mail_expert_do;
    public $ref_expert_do;
    public $cabinet_expert_cie;
    public $prenom_expert_cie;
    public $nom_expert_cie;
    public $rue1_expert_cie;
    public $rue2_expert_cie;
    public $rue3_expert_cie;
    public $cp_expert_cie;
    public $commune_expert_cie;
    public $tel_expert_cie;
    public $mail_expert_cie;
    public $ref_expert_cie;
    public $cabinet_expert_jud;
    public $rue1_expert_jud;
    public $rue2_expert_jud;
    public $rue3_expert_jud;
    public $cp_expert_jud;
    public $commune_expert_jud;
    public $prenom_expert_jud;
    public $nom_expert_jud;
    public $tel_expert_jud;
    public $mail_expert_jud;
    public $cabinet_avocat;
    public $rue1_avocat;
    public $rue2_avocat;
    public $rue3_avocat;
    public $cp_avocat;
    public $commune_avocat;
    public $tel_avocat;
    public $prenom_avocat;
    public $nom_avocat;
    public $mail_avocat;
    public $ref_avocat;

    public function __construct($row) {
        if ($row[0] != "" && $row[0] != null)
            $this->date_derniere_modif = date_create_from_format("d/m/Y h:i:s", $row[0]);
        $this->base_asqua = $row[1];
        $this->statut = $row[2];
        $this->ref_asqua = $row[3];
        $this->ref_provisoire = $row[4];
        $this->libelle = $row[5];
        $this->description = $row[6];
        $this->etat = $row[7];
        if ($row[8] != "" && $row[8] != null)
            $this->date_survenance = date_create_from_format("d/m/Y", $row[8]);
        if ($row[9] != "" && $row[9] != null)
            $this->droc = date_create_from_format("d/m/Y", $row[9]);
        $this->type_garantie = $row[10];
        $this->judiciaire = $row[11];
        $this->expertise_do = $row[12];
        $this->assureur_do = $row[13];
        $this->prenom_expert_do = $row[14];
        $this->nom_expert_do = $row[15];
        $this->rue1_expert_do = $row[16];
        $this->rue2_expert_do = $row[17];
        $this->rue3_expert_do = $row[18];
        $this->cp_expert_do = $row[19];
        $this->commune_expert_do = $row[20];
        $this->tel_expert_do = $row[21];
        $this->mail_expert_do = $row[22];
        $this->ref_expert_do = $row[23];
        $this->cabinet_expert_cie = $row[24];
        $this->prenom_expert_cie = $row[25];
        $this->nom_expert_cie = $row[26];
        $this->rue1_expert_cie = $row[27];
        $this->rue2_expert_cie = $row[28];
        $this->rue3_expert_cie = $row[29];
        $this->cp_expert_cie = $row[30];
        $this->commune_expert_cie = $row[31];
        $this->tel_expert_cie = $row[32];
        $this->mail_expert_cie = $row[33];
        $this->ref_expert_cie = $row[34];
        $this->cabinet_expert_jud = $row[35];
        $this->rue1_expert_jud = $row[36];
        $this->rue2_expert_jud = $row[37];
        $this->rue3_expert_jud = $row[38];
        $this->cp_expert_jud = $row[39];
        $this->commune_expert_jud = $row[40];
        $this->prenom_expert_jud = $row[41];
        $this->nom_expert_jud = $row[42];
        $this->tel_expert_jud = $row[43];
        $this->mail_expert_jud = $row[44];
        $this->cabinet_avocat = $row[45];
        $this->rue1_avocat = $row[46];
        $this->rue2_avocat = $row[47];
        $this->rue3_avocat = $row[48];
        $this->cp_avocat = $row[49];
        $this->commune_avocat = $row[50];
        $this->tel_avocat = $row[51];
        $this->prenom_avocat = $row[52];
        $this->nom_avocat = $row[53];
        $this->mail_avocat = $row[54];
        $this->ref_avocat = $row[55];
    }

}
