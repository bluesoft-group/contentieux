<?php

use Entities\Sinistre;
use Entities\Adresse;
use Entities\Statut_AV1;
use Entities\Statut_judiciaire;

class Sinistre_c extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut("FS");
    }

    /**
     * Récupère le sinistre dans contentieux
     * enregistre le sinistre dans le contexte
     * affiche la fiche sinistre
     *
     * @param type $id
     */
    public function index($id = null) {

        if (isset($id) && $id > 0) {
            $em = $this->doctrine->em;
            $repo = $em->getRepository('\Entities\Sinistre');
            $sinistre = $repo->find($id);
            $this->context->setSinistre($sinistre);
        }

        $sinistre = $this->context->getSinistre();
        $this->sinistre_display_statut($sinistre);
    }

    public function edit() {
        $sinistre = $this->context->getSinistre();

        if (@$_POST['date_declaration'])
            $sinistre->setDate_declaration(date_create_from_format('Y-m-d', $_POST['date_declaration']));
        if (@$_POST['description_qc'])
            $sinistre->setDescription_qc($_POST['description_qc']);
        $sinistre->setAdresse(new Adresse($_POST['rue1'], $_POST['rue2'], $_POST['rue3'], $_POST['code_postal'], $_POST['commune']));
        $sinistre->setRc((isset($_POST['RC'])));
        $sinistre->setRcd(isset($_POST['RCD']));
        if (isset($_POST['date_reception']))
            $sinistre->setDate_reception_ouvrage(date_create_from_format('Y-m-d', $_POST['date_reception']));
        if (isset($_POST['ref_asqua'])) {
            mb_internal_encoding('UTF-8');
            $ref = preg_replace("/(.*\/|[^\.a-zA-Z\d:]*)/u", '', $_POST['ref_asqua']);
            $sinistre->setRef_asqua($ref);
        }
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();


        redirect('/sinistre_c/index/');
    }

    /**
     * Création d'un nouveau sinistre
     *
     * Récupère l'affaire courante dans le contexte
     * Instancie, initialise et persiste un objet Sinistre
     *
     */
    public function nouveau_sinistre() {

        $affaire = $this->context->getAffaire();
        $this->context->setStatut("NS");
        
        $affaire->setStatut_affaire(0);
        $this->doctrine->em->persist($affaire);
        $this->doctrine->em->flush();

        if (isset($_POST['date_declaration'])) {
            $sinistre = new Sinistre();
            if (isset($_POST['tranche']))
                $sinistre->setTranche($_POST['tranche']);
            $sinistre->setDate_declaration(date_create_from_format('Y-m-d', $_POST['date_declaration']));
            @$sinistre->setAffaire($affaire);
            @$sinistre->setAdresse(new Adresse($_POST['rue1'], $_POST['rue2'], $_POST['rue3'], $_POST['code_postal'], $_POST['commune']));
            if (isset($_POST['date_reception']) && $_POST['date_reception'] != "")
                $sinistre->setDate_reception_ouvrage(date_create_from_format('Y-m-d', $_POST['date_reception']));
            @$sinistre->setRc((isset($_POST['RC'])));
            @$sinistre->setRcd(isset($_POST['RCD']));
            @$sinistre->setReception_prononcee(($_POST['reception_prononcee'] == "oui"));
            if ($_POST['statut'] == 5)
                $sinistre->setStatut(4);
            else
                $sinistre->setStatut($_POST['statut']);
            @$sinistre->setStatut_initial($_POST['statut']);
            $chrono = $this->doctrine->em->getRepository('Entities\Sinistre')->get_next_chrono($affaire);
            $sinistre->setNum_chrono($chrono);
            @$sinistre->setDescription_qc($_POST['description_qc']);

            $sinistreRepo = $this->doctrine->em->getRepository('Entities\Sinistre');
            $catrepo = $this->doctrine->em->getRepository('Entities\Categories_docs');
            switch ($sinistre->getStatut()) {
                case 1:
                    $statut = $sinistreRepo->newDo();
                    $sinistre->setDo($statut);
                    $cat_file = $catrepo->findOneBy(array('num' => 1));
                    $redirect = 'statuts/do/mails_do/declaration';
                    break;
                case 2:
                    $statut = $sinistreRepo->newAv1();
                    $sinistre->setAv1($statut);
                    $cat_file = $catrepo->findOneBy(array('num' => 2));
                    $redirect = 'statuts/av1/mails_av1/declaration';
                    break;
                case 3:
                    $statut = $sinistreRepo->newJud();
                    $sinistre->setJudiciaire($statut);
                    $cat_file = $catrepo->findOneBy(array('num' => 9));
                    $redirect = 'statuts/judiciaire/mails_jud/declaration';
                    break;
                case 4:
                    $statut = $sinistreRepo->newAmiable();
                    $sinistre->setAmiable($statut);
                    $cat_file = $catrepo->findOneBy(array('num' => 14));
                    $redirect = 'statuts/amiable/mails_amiable/declaration';
                    break;
            }
            $statut->setSinistre($sinistre);

            if (isset($_POST['ref_asqua'])) {
                $sinistre->setRef_asqua($_POST['ref_asqua']);
                $redirect = 'sinistre_c';
            }

            $this->doctrine->em->persist($sinistre);
            $this->doctrine->em->flush();

            if (isset($_FILES) && isset($_FILES['convoc'])) {
                $this->load->library('Files');
                $type = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => 101));
                $this->files->upload($_FILES['convoc'], $type, $statut);
            }

            $this->context->setSinistre($sinistre);

            if (isset($_POST['ref_asqua'])) {
                $ch = curl_init(base_url() . "asqua/synchroasqua/gLr887sQ/" . $_POST["ref_asqua"] . "/false");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $o = curl_exec($ch);
                curl_close($ch);
            }
            $this->load->helper('url');
            redirect($redirect);

            return;
        } else {
            // Recuperation des tranches dans gaia
            $tranches = null;
            if ($affaire->getId_g_affaire()) {
                $this->load->model('ImportAffaire');
                $tranches = $this->ImportAffaire->getTranches($affaire);
            }
            $this->twig->display('sinistre/nouveau_sinistre.html.twig', array(
                'new' => true,
                'topbar2' => 1,
                'tranches' => $tranches
            ));
        }
    }

    /**
     * Fait passer le sinistre actuel au statut suivant
     *
     * Récupère le statut en cours, incrémente de 1
     * Si le sinistre passe de 1 à 2, les informations de Do sont copiées dans Av1 par av1fromdo()
     * Persiste le sinistre en bdd et le met a jour dans le contexte
     *
     * @param type $id_sinistre
     */
    public function statut_suivant($statut = null) {
        $sinistre = $this->context->getSinistre();
        $statut_actuel = $sinistre->getStatut();
        if ($statut_actuel != 3) {
            if ($statut_actuel < 3) {
                if ($statut == null)
                    $sinistre->setStatut($sinistre->getStatut() + 1);
                if ($statut_actuel == 1 && $statut == "judiciaire")
                    $sinistre->setStatut($sinistre->getStatut() + 2);
            }
            if ($statut_actuel == 4)
                $sinistre->setStatut(3);
        }

        if ($sinistre->getStatut_initial() == 1) {
            switch ($sinistre->getStatut()) {
                case 2:
                    $sinistre = $this->copyFromDo($sinistre, new Statut_AV1(), 'setAv1');
                    break;
                case 3:
                    $sinistre = $this->copyFromDo($sinistre, new Statut_judiciaire(), 'setJudiciaire');
                    break;
            }
        } else {
            $jud = new Statut_judiciaire();
            $sinistre->setJudiciaire($jud);
        }
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();
        $this->sinistre_display_statut($sinistre);
    }

    /**
     * Affiche la vue du statut courant du sinistre
     *
     * @param type $sinistre
     */
    public function sinistre_display_statut($sinistre) {
        $this->load->helper('url');
        switch ($sinistre->getStatut()) {
            case 1:
                redirect('statuts/do/fiche_do/index/');
                break;
            case 2 :
                redirect('statuts/av1/fiche_av1/index/');
                break;
            case 3 :
                redirect('statuts/judiciaire/fiche_jud/index/');
                break;
            case 4 :
                redirect('statuts/amiable/fiche_amiable/index/');
                break;
            case 5:
                redirect('statuts/rp/fiche_rp');
        }
    }

    /**
     * Copie les informations de Sinistre_DO vers un nouveau statut
     *
     * @param type $sinistre
     * @return type
     */
    private function copyFromDo($sinistre, $statut, $setter) {
        $do = $sinistre->getDo();

        if ($do) {

            if ($setter != 'setAmiable') {
                if ($setter != 'setJudiciaire') {

                    $expert = $do->getContact_expert();
                    $nexpert = clone $expert;
                    $statut->setContact_expert($nexpert);

                    $cexpert = $do->getCabinet_expert();
                    $ncexpert = clone $cexpert;
                    $adresse_cexpert = $cexpert->getAdresse();
                    $nadresse_cexpert = clone $adresse_cexpert;
                    $ncexpert->setAdresse($nadresse_cexpert);
                    $statut->setCabinet_expert($ncexpert);

                    $statut->setRef_dossier_assureur($do->getRef_dossier_assureur());
                    $statut->setDate_premiere_reunion_expertise($do->getDate_premiere_reunion_expertise());
                }
            }

            $sinistre->$setter($statut);
        }
        return $sinistre;
    }

    public function historique($id = null) {
        $histRepo = $this->doctrine->em->getRepository('Entities\Historique');
        $sinistre = $this->context->getSinistre();
        if ($id == null) {
            $sinistre = $this->context->getSinistre();

            $search = $this->session->userdata("filtres_docs");

            $date = isset($_POST['date']) ? $_POST['date'] : null;
            $type = isset($_POST['type']) ? $_POST['type'] : null;
            $libelle = isset($_POST['libelle']) ? $_POST['libelle'] : null;
            $recherche = isset($_POST['recherche']) ? $_POST['recherche'] : null;
            $statut_p = isset($_POST['statut']) ? $_POST['statut'] : null;
            switch ($statut_p) {
                case 'DO':
                    $statut = $sinistre->getDo();
                    break;
                case 'AV1':
                    $statut = $sinistre->getAv1();
                    break;
                case 'JUD':
                    $statut = $sinistre->getJudiciaire();
                    break;
                case 'AMIABLE':
                    $statut = $sinistre->getAmiable();
                    break;
                case '':
                case null:
                    $statut = null;
                    break;
            }

            $search = array('statut' => $statut_p, 'date' => $date, 'type' => $type, 'libelle' => $libelle, 'recherche' => $recherche);
            $this->session->set_userdata("filtres_docs", $search);

            $hist = $histRepo->listeFiltree($sinistre, $date, $type, $libelle, $recherche, $statut);
            $this->twig->display("sinistre/historique.html.twig", array('hist' => $hist, 'topbar2' => 6, 'search' => $search));
            return;
        } else {
            $hist = $histRepo->find($id);
            $data = $hist->getData();
            $mail = $data['mail'];
            $submail = substr($mail, strpos($mail, "<table"));
            $data['mail'] = $submail;
            $fileRepo = $this->doctrine->em->getRepository('Entities\File');
            if (isset($data['attachements'])) {
                $tmp = array();
                foreach ($data['attachements'] as $attachement) {
                    if (substr($attachement, 0, 1) != "g" && substr($attachement, 0, 1) != "q") {
                        $tmp[] = $fileRepo->find($attachement);
                    } else {
                        $this->load->model('ImportDocuments');
                        if (substr($attachement, 0, 1) == "g")
                            $tmp[] = $this->ImportDocuments->getDoc(substr($attachement, 1));
                        if (substr($attachement, 0, 1) == "q")
                            $tmp[] = $this->ImportDocuments->getDocQp(substr($attachement, 1));
                    }
                }
                $data['attachements'] = $tmp;
            }
            $hist->setData($data);
            $this->twig->display("sinistre/vue_mail_histo.html.twig", array('mail' => $hist, 'topbar2' => 2, 'active' => 0));
        }
    }

    public function desactiver($id) {
        $user = $this->context->getUser();
        if ($user->role != 10 && $user->role != 8)
            die("Vous n'avez pas acces à cette fonctionnalité");
        $repo = $this->doctrine->em->getRepository('Entities\Sinistre');
        $sinistre = $repo->find($id);
        $sinistre->setFlag_sinistre_desactive(true);
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();
        $this->context->setSinistre(null);
    }

    public function import_sinistre() {
        $this->twig->display('sinistre/import_sinistre.html.twig', array('topbar2' => 4));
    }

    public function nouveau_rp() {

        $affaire = $this->context->getAffaire();
        $this->context->setStatut("NS");

        if (!empty($_POST)) {
            $sinistre = new Sinistre();
            if (isset($_POST['tranche']))
                $sinistre->setTranche($_POST['tranche']);
            @$sinistre->setAffaire($affaire);
            @$sinistre->setAdresse(new Adresse($_POST['rue1'], $_POST['rue2'], $_POST['rue3'], $_POST['code_postal'], $_POST['commune']));
            if (isset($_POST['date_reception']) && $_POST['date_reception'] != "")
                $sinistre->setDate_reception_ouvrage(date_create_from_format('Y-m-d', $_POST['date_reception']));
            @$sinistre->setReception_prononcee(($_POST['reception_prononcee'] == "oui"));
            $sinistre->setStatut(5);
            @$sinistre->setStatut_initial(5);
            $chrono = $this->doctrine->em->getRepository('Entities\Sinistre')->get_next_chrono($affaire);
            $sinistre->setNum_chrono($chrono);

            $sinistreRepo = $this->doctrine->em->getRepository('Entities\Sinistre');

            $statut = $sinistreRepo->newRp();
            $sinistre->setRp($statut);
            $redirect = 'statuts/rp/mails_rp';

            $statut->setSinistre($sinistre);

            $this->doctrine->em->persist($sinistre);
            $this->doctrine->em->flush();

            $this->context->setSinistre($sinistre);

            $this->load->helper('url');
            redirect($redirect);

            return;
        } else {
            // Recuperation des tranches dans gaia
            $tranches = null;
            if ($affaire->getId_g_affaire()) {
                $this->load->model('ImportAffaire');
                $tranches = $this->ImportAffaire->getTranches($affaire);
            }
            $this->twig->display('sinistre/nouveau_rp.html.twig', array(
                'new' => true,
                'topbar2' => 5,
                'tranches' => $tranches
            ));
        }
    }

    public function liste() {
        $affaire = $this->context->getAffaire();
        $sinistres = $affaire->getSinistres();
        $rps = $affaire->getRps();
        $this->twig->display('sinistre/liste.html.twig', array('sinistres' => $sinistres, 'rps' => $rps));
    }

    public function cloturer_sinistre($id_sinistre) {

        /* @var $sinistre Sinistre */
        $sinistre = $this->doctrine->em->getRepository('Entities\Sinistre')->find($id_sinistre);

        $sinistre->setSinistre_clos_date(date_create_from_format("Y-m-d", $_POST["date_cloture"]));
        $sinistre->setSinistre_clos_cout($_POST["cout"]);
        $sinistre->setSinistre_clos_cout_qc($_POST["cout_qc"]);
        $sinistre->setSinistre_clos_mission($_POST["mission"]);
        $sinistre->setSinistre_clos_destination_ouvrage($_POST["destination"]);
        $sinistre->setSinistre_clos_partie_ouvrage($_POST["partie_ouvrage"]);
        $sinistre->setSinistre_clos_phase_concernee($_POST["phase"]);
        
        $sinistre->setFlag_sinistre_desactive(2);
        
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();
    }
    
    public function annuler_cloture($id_sinistre) {
        /* @var $sinistre Entities\Sinistre */
        $sinistre = $this->doctrine->em->getRepository('Entities\Sinistre')->find($id_sinistre);
        $sinistre->setFlag_sinistre_desactive(0);
        
        /* @var $affaire Entities\Affaire */
        $affaire = $sinistre->getAffaire();
        $affaire->setStatut_affaire(0);
        $this->doctrine->em->persist($affaire);
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();
    }

}
