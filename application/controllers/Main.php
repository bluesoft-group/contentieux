<?php

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User');
    }

    /**
     * Récupère l'objet utilisateur en session
     * Vérifie si l'utilisateur existe ( = authentifié )
     * Si non, l'utilisateur est redirigé vers /Authentification/login
     * Si oui, affiche l'ecran d'accueil
     */
    public function index()
    {
        $user = $this->session->userdata('user');

        if ($user == null)
        {
            $this->login();
            return;
        }
        $num_affaire = null;
        $search = $this->context->getSearch();
        $recherche = (isset($search["recherche"])) ? $search["recherche"] : null;
        $agence = (isset($search["agence"])) ? $search["agence"] : null;
        $statuts = (isset($search["statuts"])) ? $search["statuts"] : null;
        $statut_affaire_1 = (isset($search["statut_affaire_1"])) ? $search["statut_affaire_1"] : false;

        $affaireRepo = $this->doctrine->em->getRepository('Entities\Affaire');
        $affaires = $affaireRepo->search($num_affaire, $recherche, $user, $agence, $statuts, $statut_affaire_1);
        @$suppleance = $user->remplace;

        if ($suppleance)
        {
            foreach ($suppleance as $suppleant)
            {
                $affaires_supp = $affaireRepo->search($num_affaire, $recherche, $suppleant, $agence, $statuts, $statut_affaire_1);
                foreach ($affaires_supp as $affaire_supp)
                {
                    if (!in_array($affaire_supp, $affaires))
                    {
                        $affaires[] = $affaire_supp;
                    }
                }
            }
        }

        $this->load->model('Utils');
        $agences = $this->Utils->getAgences($user);

        $this->twig->display('global/accueil.html.twig', array('affaires' => $affaires, 'agences' => $agences, 'topbar2' => 1));
    }

    public function ldap_login($username, $password)
    {
        try
        {
            // Ouvre une connexion vers le serveur LDAP
            $ldapconn = ldap_connect("ldap://10.1.0.3");
            $isAuth = false;

            ldap_set_option($ldapconn, LDAP_OPT_NETWORK_TIMEOUT, 4);
            ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);


            // On teste si le password n'est pas vide, pour ne pas 
            if ($ldapconn && $username != '' && $password != '')
            {
                // Teste le username et le password permet l'authentification de l'utilisateur
                $isAuth = ldap_bind($ldapconn, $username . '@qualigroup', $password);
            }

            if (!$isAuth)
            {
                $ldapconn = ldap_connect("ldap://10.1.0.1");

                ldap_set_option($ldapconn, LDAP_OPT_NETWORK_TIMEOUT, 4);
                ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

                $isAuth = ldap_bind($ldapconn, $username . '@qualigroup', $password);
            }

            ldap_close($ldapconn);
        } catch (\Exception $ex)
        {
            return false;
        }
        return $isAuth;
    }

    /**
     * Tente d'authentifier l'utilisateur et enregistre le profil en session
     *
     */
    public function login()
    {

        $login = (isset($_POST["id"])) ? $_POST["id"] : null;
        $pass = (isset($_POST["mdp"])) ? $_POST["mdp"] : null;

        $user = null;

        if ($login == null || $pass == null)
        {
            $this->twig->display('login.html.twig', array('panel_title' => 'Outil Contentieux'));
            return;
        }

        if ($login == "nadege.jardin" && $pass == "ml9lgu")
        {
            $user = new stdClass();
            $user->id_utilisateur = 1385;
            $user->id_service = 0;
            $user->agence = array(0);
            $user->region = array(0);
            $user->nom = "JARDIN";
            $user->prenom = "Nadège";
            $user->email = "nadege.jardin@asqua.fr";
            $user->civilite = 2;
            $user->initiales = "NJ";
            $user->role = 8;
            $user->role_str = "Asqua";
            $user->role_libelle = "Asqua";
        } else
        {
            if ($this->ldap_login($login, $pass))
            {
                $user = $this->User->get($login);
            }
        }
        if ($user && $user->role != '')
        {
            $this->session->set_userdata('user', $user);
            log_message("INFO", "Utilisateur connecté : " . $user->prenom . ' ' . $user->nom);
            redirect(base_url());
            return;
        }
        if (!$user)
            $message = "Echec d'authentification";
        else if ($user->role == null)
        {
            $message = "Erreur d'attribution des droits";
            ob_start();
            var_dump($user);
            $dump = ob_get_clean();
            $this->load->helper('mailer');
            mailSupport("$login <br><br> $dump", $message);
        }

        $this->twig->display('login.html.twig', array('panel_title' => 'Erreur de connexion', 'message' => $message));
    }

    /**
     * Déconnecte l'utilisateur
     *
     * La session est détruite et l'utilisateur est redirigé vers /Main
     */
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('');
    }

    public function suppleance()
    {
        $user = $this->context->getUser();
        $suppleance = $this->doctrine->em->getRepository('Entities\Suppleance')->findOneBy(array('id_utilisateur' => $user->id_utilisateur));

        if (isset($_POST['date_debut']))
        {
            if (!$suppleance)
                $suppleance = new Entities\Suppleance();
            $suppleance->setDate_debut(date_create_from_format('Y-m-d', $_POST['date_debut']));
            if (isset($_POST['date_fin']) && $_POST['date_fin'] != "")
                $suppleance->setDate_fin(date_create_from_format('Y-m-d', $_POST['date_fin']));

            $suppleance->setId_utilisateur($user->id_utilisateur);
            $suppleance->setId_suppleant($_POST['id_suppleant']);
            $suppleance->setNom_suppleant($_POST['nom_suppleant']);

            $this->doctrine->em->persist($suppleance);
            $this->doctrine->em->flush();
        }

        $this->twig->display('suppleance.html.twig', array('suppleance' => $suppleance));
    }

    public function supp_suppleance()
    {
        $user = $this->context->getUser();
        $suppleance = $this->doctrine->em->getRepository('Entities\Suppleance')->findOneBy(array('id_utilisateur' => $user->id_utilisateur));

        $this->doctrine->em->remove($suppleance);
        $this->doctrine->em->flush();

        redirect(base_url('main/suppleance'));
    }

}
