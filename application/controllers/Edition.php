<?php

class Edition extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getStatut($id) {
        $editions_repo = $this->doctrine->em->getRepository('Entities\Edition');
        $edition = $editions_repo->find($id);
        $statut = $edition->getEtat();

        echo $statut;
        return;
    }

    public function validate($id) {
        $editions_repo = $this->doctrine->em->getRepository('Entities\Edition');
        $edition = $editions_repo->find($id);
        $doc = $edition->getDocument();
        if (file_exists($doc->getUri())) {
            unlink($doc->getUri());
        }

        $edition->setDocument(null);
        $this->doctrine->em->remove($doc);
        $this->doctrine->em->flush();

        $this->load->library('AsposeLib');

        $this->asposelib->make_pdf($edition->getType_aspose(), json_decode($edition->getData()), $edition->getStatut(), $edition->getNom_tmp(), $edition, false);
    }

    public function annuler($id) {
        $editions_repo = $this->doctrine->em->getRepository('Entities\Edition');
        $edition = $editions_repo->find($id);

        $doc = $edition->getDocument();
        if (@$doc && file_exists($doc->getUri())) {
            unlink($doc->getUri());
            $edition->setDocument(null);
            $this->doctrine->em->remove($doc);
        }
        $edition->setEtat(-1);

        $this->doctrine->em->persist($edition);
        $this->doctrine->em->flush();
    }

    public function diffuser($id) {
        $this->load->library('AsposeLib');
        $this->asposelib->diffuser($id);
    }
}
