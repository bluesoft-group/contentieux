<?php

/**
 * Google API (gmail) key :
 * AIzaSyC9vlJUDgmqOJSlbN1w2BLsJlZvEFax5EM
 */
class Import_mail extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    public function getGoogle() {
        $this->load->library('Google');
        $google = $this->google->getClient();

        $access_token = $this->session->userdata('google_token');

        if (!isset($access_token)) {
            if (isset($_GET['code'])) {
                $google->authenticate($_GET['code']);
                $access_token = $google->getAccessToken();
                $this->session->set_userdata('google_token', $access_token);
            } else {
                $auth_url = $google->createAuthUrl();
                redirect($auth_url);
            }
        }

        $google->setAccessToken($access_token);

        return $google;
    }

    public function index() {

        $sinistre = $this->context->getSinistre();
        switch ($sinistre->getStatut()) {
            case 1:
                $topbar3 = 'interne/do/topbar_interne.html.twig';
                break;
            case 2:
                $topbar3 = 'interne/av1/topbar_interne.html.twig';
                break;
            case 3:
                $topbar3 = 'interne/judiciaire/topbar_interne.html.twig';
                break;
            case 4:
                $topbar3 = 'interne/amiable/topbar_interne.html.twig';
        }

        $google = $this->getGoogle();

        $mail_service = new Google_Service_Gmail($google);
        $labels = $mail_service->users_labels->listUsersLabels('me');
        $user_labels = array();
        foreach ($labels as $k => $v) {
            if ($v->type == "user") {
                $user_labels[] = $mail_service->users_labels->get('me', $v->id);
            }
        }


        $this->twig->display('import_mails.html.twig', array('topbar2' => 5, 'topbar3' => $topbar3, 'active' => 3, 'labels' => $user_labels));
    }

    public function getMailsFromLabel($label) {
        $access_token = $this->session->userdata('google_token');

        if (!isset($access_token))
            echo "nonAuth";

        $google = $this->getGoogle();
        $mail_service = new Google_Service_Gmail($google);
        $mails = $mail_service->users_messages->listUsersMessages('me', array('labelIds' => $label));
        $user_mails = array();

        foreach ($mails as $mail) {
            $mail_light = array();
            $mail_light['id'] = $mail->id;
           // $mail['subject'] = $mail->payload->headers->name;

            $user_mails[] = $mail_light;
        }
        
        var_dump($user_mails);
        echo "<br><br>";
        var_dump($mails);
    }

}
