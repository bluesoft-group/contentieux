<?php

class FC extends MY_Controller {
    
    private $qualiperf;
    
    public function __construct() {
        
        parent::__construct();
    }
    
    public function index() {
        
        $this->qualiperf = $this->load->database('qualiperf', true);
        
        $query = <<<SQL
            SELECT DISTINCT b.* FROM utilisateurs_services a INNER JOIN utilisateurs b ON a.id_utilisateur=b.id_utilisateur 
            WHERE 
            id_service IN (SELECT id_service FROM services WHERE _id_agence IN (SELECT _id_agence FROM services WHERE id_service = '729') ) 
            AND (date_sortie IS NULL OR date_sortie > getDate()) 
            AND id_profil IN (3, 4, 5, 6) 
            ORDER BY b.nom ASC
SQL;

        $result = $this->qualiperf->query($query)->result_array();
        
        echo count($result);
    }
}