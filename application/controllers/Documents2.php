<?php

class Documents extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function enregistrer_filtres() {
        $filtres = $_POST["data"];
        $this->session->set_userdata("filtres_docs", $filtres);
    }

    public function gestion() {
        $docs_affaire = null;
        $docs_sinistre = null;

        //upload
        if (isset($_POST['nom'])) {
            ini_set('upload_max_filesize', '20M');
            $typesRepo = $this->doctrine->em->getRepository('Entities\Type_docs');
            $sinistre = $this->context->getSinistre();
            $statut = null;
            if ($sinistre)
                $statut = $this->context->getSinistre()->get_current_statut();
            $affaire = $this->context->getAffaire();
            $this->load->library('Files');
            $file_type = $typesRepo->find($_POST['type']);
            $date = null;
            $date_reception = null;
            if (isset($_POST['date']))
                $date = date_create_from_format('Y-m-d', $_POST['date']);
            if (isset($_POST['date_reception']) && $_POST['date_reception'] != "")
                $date_reception = date_create_from_format('Y-m-d', $_POST['date_reception']);
            else
                $date_reception = new DateTime();

            $file = $this->files->upload($_FILES['userfile'], $file_type, $statut, $affaire, $date, $date_reception, $_POST["nom"]);
        }



        $docs_affaire1 = $this->context->getAffaire()->getDocuments()->toArray();
        if (!isset($docs_affaire1) || $docs_affaire1 == null || !is_array($docs_affaire1))
            $docs_affaire1 = array();


        //recupération gaïa
        $docs_gaia = $this->getDocs_gaia($this->context->getAffaire());

        // fin gaïa
        if (isset($docs_gaia) && is_array($docs_gaia))
            $docs = array_merge($docs_affaire1, $docs_gaia);

        if ($this->context->getSinistre())
            $docs_sinistre = $this->context->getSinistre()->getDocuments();

        $typesRepo = $this->doctrine->em->getRepository('Entities\Type_docs');
        $types_docs = $typesRepo->findAll();

        $catRepo = $this->doctrine->em->getRepository('Entities\Categories_docs');
        $cat_docs = $catRepo->findAll();

        $filtres = $this->session->userdata("filtres_docs");

        foreach ($docs_sinistre as $doc_sinistre) {
            $docs[] = $doc_sinistre;
        }

        $this->twig->display('documents/list.html.twig', array("docs" => $docs, "types" => $types_docs, "categories" => $cat_docs, "filtres" => $filtres, "topbar2" => "documents"));
    }

    public function getDocs_gaia($affaire) {
        $this->load->model('importDocuments');
        $docs_gaia = $this->importDocuments->getDocs($affaire, "all");
        $docs_collection = array();
        $typesRepo = $this->doctrine->em->getRepository('Entities\Type_docs');
        if (is_array($docs_gaia) && count($docs_gaia) > 0)
            foreach ($docs_gaia as $doc) {
                $mydoc = new \Entities\File();
                $mydoc->setId("g" . $doc["id_rapport"]);
                $mydoc->setDate(date_create_from_format("Y-m-d h:i:s", $doc["rap_date"]));
                $mydoc->setNom($doc["rap_libelle"]);
                $mydoc->setNom_public($doc["rap_libelle"]);
                $mydoc->setSize($doc["rap_taille_octets"]);

                $type = $typesRepo->findOneBy(array("num" => $doc["rap_type"]));
                $mydoc->setType($type);

                //$categorie = $type->getCategorie();

                $docs_collection[] = $mydoc;
            }

        return $docs_collection;
    }

    public function download($id) {
        if ($id[0] != "g" && $id[0] != "q") {
            $fileR = $this->doctrine->em->getRepository('Entities\File');
            $file = $fileR->find($id);
        }
        if ($id[0] == "g") {
            $id = str_replace("g", "", $id);
            $this->load->model('ImportDocuments');
            $file = $this->ImportDocuments->getDoc($id);
            $tmp = $this->getByCurl($file->getUri());
            $file->setUri($tmp);
        }
        if ($id[0] == "q") {
            $id = str_replace("q", "", $id);
            $this->load->model('ImportDocuments');
            $file = $this->ImportDocuments->getDocQp($id);
            $tmp = $this->getByCurl($file->getUri());
            $file->setUri($tmp);
        }
        log_message('DEBUG', $file->getUri());
        if (!$file) {
            echo 'erreur';     //exceptionme
            return;
        } else {
            $data = $file->getUri();
            ignore_user_abort(true);
            set_time_limit(300); // disable the time limit for this script
            if ($fd = fopen($data, "r")) {
                $fsize = filesize($data);
                $path_parts = pathinfo($data);
                header("Content-type: application/octet-stream");
                header("Content-Disposition: filename=\"" . $file->getNom_public() . "." . $file->getExt() . "\"");
                header("Content-length: $fsize");
                header("Cache-control: private");
                while (!feof($fd)) {
                    $buffer = fread($fd, 1024);
                    echo $buffer;
                }
                fclose($fd);
            }
            $id = $file->getId();
            if ($id[0] == "q" || $id[0] == "g") {
                unlink($file->getUri());
            }
            exit;
        }
    }

    public function acces_rapide() {

        $documents = false;
        $error = false;
        $result = array();
        if (isset($_POST['num_affaire'])) {
            $this->load->model('importDocuments');
            $this->load->model('importAffaire');

            $affaires = $this->importAffaire->checkInQualiperf($_POST['num_affaire']);
            if (!isset($affaires[0])) {
                $error = "Impossible de trouver l'affaire";
            } else {
                $affaire_tmp = $affaires[0];
                $affaire = $this->importAffaire->importQualiperf($affaire_tmp->code);
            }
            if (!isset($affaire) || !$affaire) {
                $error = "Erreur de récuperation de l'affaire";
            } else if ($affaire && !$affaire->getId_g_affaire()) {
                $error = "Affaire non trouvée dans Gaïa";
            } else {
                $documents = $this->importDocuments->getDocs($affaire, 'all');
                foreach ($documents as $doc) {
                    $type_g = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => $doc["rap_type"]));
                    $date_d = date_create_from_format('Y-m-d H:i:s', $doc["rap_date"]);
                    $file = new Entities\File();
                    $file->setExt("pdf");
                    $file->setNom($doc["rap_filename"]);
                    $file->setUpload_date($date_d);
                    $file->setDate($date_d);
                    $file->setType($type_g);
                    $file->setUri("http://gaia.qualigroup.net/gaia/DownloadRapport?get=" . $doc["rap_publicKey"]);
                    $file->setId("g" . $doc["id_rapport"]);

                    $result[] = $file;
                }
            }
        }

        $this->twig->display('documents/acces_rapide.html.twig', array('error' => $error, 'documents' => $result));
    }

    public function renommer($id) {

        if (!isset($_POST) || !isset($_POST["nom"]))
            die("erreur");

        $nom = $_POST['nom'];
        $fr = $this->doctrine->em->getRepository('Entities\File');
        $file = $fr->find($id);

        if ($file) {
            $file->setNom($nom);
            $this->doctrine->em->persist($file);
            $this->doctrine->em->flush();
        }
    }

    public function supprimer($id) {
        $fr = $this->doctrine->em->getRepository('Entities\File');
        $file = $fr->find($id);

        if ($file) {
            $this->doctrine->em->remove($file);
            $this->doctrine->em->flush();
        }
    }

    public function lier_affaire($id) {
        $affaire = $this->context->getAffaire();

        $fr = $this->doctrine->em->getRepository('Entities\File');
        $file = $fr->find($id);

        $file->setSinistre(null);
        $affaire->addDocument($file);

        $this->doctrine->em->persist($file);
        $this->doctrine->em->persist($affaire);
        $this->doctrine->em->flush();
    }

    public function lier_sinistre($id) {
        $sinistre = $this->context->getSinistre();

        $fr = $this->doctrine->em->getRepository('Entities\File');
        $file = $fr->find($id);

        $file->setSinistre($sinistre);
        $file->setAffaire(null);

        $this->doctrine->em->persist($file);
        $this->doctrine->em->flush();
    }

    public function flagger($id) {
        $sinistre = $this->context->getSinistre();
        $docs_flagges = $sinistre->getFlag_documents();
        if (in_array($id, $docs_flagges)) {
            $k = array_search($id, $docs_flagges);
            unset($docs_flagges[$k]);
        } else {
            $docs_flagges[] = $id;
        }

        $sinistre->setFlag_documents($docs_flagges);

        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();
    }

    public function getByCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $uniq = uniqid();
        $uri = BASEPATH . "../files/tmp/" . $uniq;
        file_put_contents($uri, $output);
        return $uri;
    }

    public function getList() {
        $sinistre = $this->context->getSinistre();
        $repo = $this->doctrine->em->getRepository('Entities\File');

        $docs = $repo->getDocs($sinistre);
        $data = array();
        foreach ($docs as $doc) {
            $data[$doc->getId()] = $doc->toArray();
        }

        echo json_encode($data);
    }

    public function getTypesList() {
        $repo = $this->doctrine->em->getRepository('Entities\Type_docs');
        $types = $repo->findAll();

        $data = array();
        foreach ($types as $type) {
            $data[] = array($type->getNum(), $type->getType());
        }

        echo json_encode($data);
    }

    public function getType($num) {
        $type = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => $num));
        echo $type->getType();
    }

    public function filtrer() {

        $recherche = @$_POST['recherche'];
        $docs_gaia = (@$_POST['docs_gaia'] == "true");
        $docs_ctx = (@$_POST['docs_ctx'] == "true");
        $type = null;

        if ($_POST['type'] !== '')
            $type = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => $_POST['type']));
        $sinistre = $this->context->getSinistre();

        $repo = $this->doctrine->em->getRepository('Entities\File');
        $docs = $repo->getDocs($sinistre, $type, $docs_gaia, $docs_ctx);

        $data = array();
        foreach ($docs as $doc) {
            $id = $doc->getId();
            if ($recherche != null && !strstr(strtolower($doc->getNom()), strtolower($recherche)))
                continue;
            $data[$doc->getId()] = $doc->toArray();
        }


        echo json_encode($data);
    }

    public function preview($id, $byEdition = false) {
        if ($byEdition != false) {
            $edRepo = $this->doctrine->em->getRepository('Entities\Edition');
            $edition = $edRepo->find($id);
            $id = $edition->getDocument()->getId();
        }

        $fileR = $this->doctrine->em->getRepository('Entities\File');
        $file = $fileR->find($id);
        if (!$file) {
            echo 'erreur';     //exceptionme
            return;
        } else {
            $data = $file->getUri();
            ignore_user_abort(true);
            set_time_limit(0); // disable the time limit for this script
            if ($fd = fopen($data, "r")) {
                $fsize = filesize($data);
                $path_parts = pathinfo($data);
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=\"" . $file->getNom() . "\"");
                header("Content-length: $fsize");
                header("Cache-control: private");
                while (!feof($fd)) {
                    $buffer = fread($fd, 2048);
                    echo $buffer;
                }
                fclose($fd);
            }
            exit;
        }
    }

    public function changer_type($id, $num) {
        $this->load->model("Documents_model");
        $this->Documents_model->changer_type_doc($id, $num);
    }

}
