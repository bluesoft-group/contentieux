<?php

use Entities\Affaire;
use Entities\Sinistre;

/**
 * Stocke en session les entités
 * Stocke l'id et recupere l'entite depuis l'id a la demande
 */
class Context {

    private $CI;
    private $em;
    public $statut;

    /**
     * Constructeur
     *
     * Récupère l'instance CI et l'EntityManager de doctrine
     */
    public function __construct() {
        $this->CI = & get_instance();
        $this->em = $this->CI->doctrine->em;
        log_message('info', 'Context library loaded');
    }

    /**
     * Récupère les entités Affaire et sinistre avec les id stockés en session
     * Renvoie le tableau context
     *
     * @return array
     */
    public function get() {
        $user = $this->CI->session->userdata('user');
        $context = $this->CI->session->userdata('context');
        if (isset($context['affaire'])) {
            $affaire = $this->getAffaire();
            $context['affaire'] = $affaire;
        }
        if (isset($context['sinistre'])) {
            $sinistre = $this->getSinistre();
            $context['sinistre'] = $sinistre;
        }
        if (isset($context['rp'])) {
            $rp = $this->getRp();
            $context['rp'] = $rp;
        }
        $context['user'] = $user;
        return $context;
    }

    /**
     * Enregistre l'id de $affaire dans la session
     * Si l'id de l'affaire à changé, sinistre est remis à zero
     *
     * @param Affaire $affaire
     */
    public function setAffaire($affaire) {
        $context = $this->CI->session->userdata('context');
        if ($affaire instanceof Affaire) {
            if (!isset($context['affaire']) || $context['affaire'] != $affaire->getId()) {
                $context['affaire'] = $affaire->getId();

                $sinistres = $affaire->getSinistres();
                if (count($sinistres) == 1) {
                    $this->setSinistre($sinistres[0]);
                    $this->setStatut($sinistres[0]->getStatut());
                } else {
                    $context['sinistre'] = null;
                    $context['sinistreIndex'] = null;
                }
            }
        } else {
            $context['affaire'] = null;
            $context['sinistre'] = null;
            $context['sinistreIndex'] = null;
        }
        $this->CI->session->set_userdata('context', $context);
    }

    /**
     * Récupère l'id de l'affaire en session et hydrate l'entité correspondante (Affaire)
     *
     * @return Affaire
     */
    public function getAffaire() {
        $context = $this->CI->session->userdata('context');
        if (isset($context['affaire']) && $context['affaire'] != null) {
            $affaire = $this->em->getRepository('Entities\Affaire')->find($context['affaire']);
            return $affaire;
        } else {
            return null;
        }
    }

    /**
     * Vérifie la position de $sinistre dans l'affaire et change la valeur sinistreIndex du context
     *
     * @param Sinistre $sinistre
     */
    private function setSinistreIndex(Sinistre $sinistre) {
        $context = $this->CI->session->userdata('context');
        $affaire = $this->getAffaire();
        $num_sin = $affaire->getSinistres()->indexOf($sinistre);
        $context['sinistreIndex'] = $num_sin + 1;
        $this->CI->session->set_userdata('context', $context);
    }

    /**
     * Enregistre l'id de $sinistre dans la session
     *
     * @param Sinistre $sinistre
     */
    public function setSinistre($sinistre) {
        $context = $this->CI->session->userdata('context');
        $context['sinistre'] = null;
        if ($sinistre != null) {
            $context['sinistre'] = $sinistre->getId();
        }
        $this->CI->session->set_userdata('context', $context);
    }

    /**
     * Récupère l'id du sinistre en session et hydrate l'entité correspondante (Sinistre)
     *
     * @return Sinistre
     */
    public function getSinistre() {
        $context = $this->CI->session->userdata('context');
        if (isset($context['sinistre']) && $context['sinistre'] != null && $context['sinistre'] > 0)
            $sinistre = $this->em->getRepository('Entities\Sinistre')->find($context['sinistre']);
        else
            return false;
        return $sinistre;
    }

    public function setStatut($statut) {
        switch ($statut) {
            case 1:
                $statut = "DO";
                break;
            case 2:
                $statut = "AV1";
                break;
            case 3:
                $statut = "judiciaire";
                break;
            case 4:
                $statut = "amiable";
                break;
        }

        $context = $this->CI->session->userdata('context');
        $context['statut'] = $statut;
        $this->statut = $statut;
        $this->CI->session->set_userdata('context', $context);
    }

    public function getStatut() {

        $context = $this->CI->session->userdata('context');
        $statut = $context['statut'];


        return $statut;
    }

    public function getStatut_e() {
        $sinistre = $this->getSinistre();
        $statut_int = $sinistre->getStatut();

        switch ($statut_int) {
            case 1:
                $statut = $sinistre->getDo();
                break;
            case 2:
                $statut = $sinistre->getAv1();
                break;
            case 3:
                $statut = $sinistre->getJudiciaire();
                break;
            case 4:
                $statut = $sinistre->getAmiable();
                break;
            case 5:
                $statut = $sinistre->getRp();
                break;
        }

        return $statut;
    }

    /*
     * Enregistre en session les filtres sur les affaires de la page d'accueil
     */

    public function setSearch($search) {
        $context = $this->CI->session->userdata('context');
        $context['search'] = $search;
        $this->CI->session->set_userdata('context', $context);
    }

    public function getSearch() {
        $context = $this->CI->session->userdata('context');
        $search = isset($context['search']) ? $context['search'] : null;
        return $search;
    }

    public function getUser() {
        $user = $this->CI->session->userdata('user');
        return $user;
    }

    public function setRp($rp) {
        $context = $this->CI->session->userdata('context');
        $context['rp'] = null;
        if ($rp != null) {
            $context['rp'] = $rp->getId();
        }
        $this->CI->session->set_userdata('context', $context);
    }

    public function getRp() {
        $context = $this->CI->session->userdata('context');
        $rp = $context['rp'];
        if ($rp != null) {
            return $this->CI->doctrine->em->getRepository('Entities\Rp')->find($rp);
        } else {
            return $rp;
        }
    }

    public function setEditable($bool) {
        $context = $this->CI->session->userdata('context');
        $context['edit'] = $bool;
        $this->CI->session->set_userdata('context', $context);
    }

}
