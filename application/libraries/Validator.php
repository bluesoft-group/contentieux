<?php

use Entities\Statut_DO;
use Entities\Statut_AV1;
use Entities\Statut_amiable;
use Entities\Statut_judiciaire;

class Validator {

    /**
     * Valide une entité (objet), accède à ses propriétés définies dans le tables
     * ( forme : array("prop" => "libellé", "prop.sous_prop" => "libellé") )
     * Renvoie un tableau des libellés dont les propriétés sont nulles
     *
     * @param Entity $entity
     * @param array $props
     * @return array
     */

    public function validate($entity, array $props) {
        $manquants = array();
        foreach ($props as $prop => $msg) {
            $prop_arr = explode(".", $prop);
            foreach ($prop_arr as $k => $elem) {
                $prop_arr[$k] = "get" . ucfirst($elem) . "()";
                $getter = implode("->", $prop_arr);
            }
            $str = 'return $entity->' . $getter . ';';
            $value = eval($str);
            if ($value == null || $value == "") {
                $manquants[] = $msg;
            }
        }
        
        return $manquants;
    }

}
