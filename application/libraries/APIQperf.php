<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class APIQperf {

    private $url = "https://api-qualiperf.qualigroup.net/";
    private $user = "app";
    private $password = "32A057E6-77A0-414C-A258-FD51A7008624";
    private $file_auth;
    private $infos_auth;

    public function __construct() {

        $this->file_auth = APPPATH . "config/qperf.json";

        $last_auth = file_get_contents($this->file_auth);
        $this->infos_auth = json_decode($last_auth, true);

        if ($last_auth == false) {
            echo "Auth . \r\n";
            $this->auth();
        } else {
            $expiration = $this->infos_auth[".expires"];
            $date_expiration = date_create_from_format("D, d M Y H:i:s e", $expiration);
            if ($date_expiration < new DateTime("now")) {
                $this->auth();
            }
        }
    }

    public function auth() {
        $ch = curl_init();

        $post = array(
            'grant_type' => 'password',
            'UserName' => $this->user,
            'password' => $this->password
        );

        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        $body = http_build_query($post);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->url . "token");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $output = curl_exec($ch);
        echo $output;
        curl_close($ch);

        file_put_contents($this->file_auth, $output);
        $this->infos_auth = json_decode($output);
    }

    public function get_infos_contrat($id_contrat) {

        $ch = curl_init();

        $headers = array(
            'Authorization: Bearer ' . $this->infos_auth["access_token"]
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->url . "ws/Documents/contrats/" . $id_contrat);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output, true);
    }

    public function get_document($id_doc) {
        $ch = curl_init();

        $headers = array(
            'Authorization: Bearer ' . $this->infos_auth["access_token"]
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->url . "ws/Documents/" . $id_doc);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $output = curl_exec($ch);
        curl_close($ch);
        
        return $output;
    }

}
