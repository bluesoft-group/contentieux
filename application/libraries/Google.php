<?php

require APPPATH . 'third_party/vendor/autoload.php';

Class Google {

    public function __construct() {
        define('APPLICATION_NAME', 'Gmail API Quickstart');
        define('CREDENTIALS_PATH', 'gmail-api-quickstart.json');
        define('CLIENT_SECRET_PATH', 'client_secret.json');
        define('SCOPES', implode(' ', array(
            Google_Service_Gmail::GMAIL_READONLY, Google_Service_Gmail::GMAIL_LABELS)
        ));
    }

    public function getClient() {
        $client = new Google_Client();
        $client->setAuthConfigFile(CLIENT_SECRET_PATH);
        $client->setRedirectUri('http://mlb.qualigroup.net/gaia_sinistre/import_mail');
        $client->setScopes(SCOPES);
        
        return $client;
    }

}
