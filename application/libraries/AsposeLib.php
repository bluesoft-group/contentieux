<?php

class AsposeLib {

    private $ci;
    private $uploaded_path;

    public function __construct() {
        $this->ci = &get_instance();
        $this->uploaded_path = $this->ci->config->item("path_aspose");
    }

    //Type = type pour le service Aspose (distinction CR reunion DO, AV1 ...)
    //Type_e = Type pour contentieux (table type_docs)
    public function make_pdf($type, $data, $statut, $nom_fichier, \Entities\Edition $edition = null, $preview = true) {
        $sinistre = $this->ci->context->getSinistre();
        $req = new Entities\Service_aspose();
        $req->setSinistre($sinistre);
        $req->setType($type);
        $req->setNom_fichier($nom_fichier);
        if ($statut)
            $req->setStatut($statut);
        $req->setPreview($preview);
        $req->setData(json_encode($data));
        if ($edition) {
            $req->setEdition($edition);
            $edition->setEtat(0);
            $this->ci->doctrine->em->persist($edition);
        }
        $this->ci->doctrine->em->persist($req);
        $this->ci->doctrine->em->flush();
    }

    public function getAns($id, $guid) {

        ob_start();
//        ini_set("display_errors", true);
//        error_reporting(E_ALL);

        $this->ci->load->library('Files');
        $sv_rep = $this->ci->doctrine->em->getRepository('Entities\Service_aspose');
        $req = $sv_rep->find($id);
        $edition = $req->getEdition();
        $sinistre = $this->ci->context->getSinistre();
        if ($req && $req->getGuid() == $guid) {
            if (!file_exists($this->uploaded_path . $guid . ".pdf")) {
                echo "Fichier introuvable";
                log_message("ERROR", "Erreur de reception aspose");
                return;
            } else {
                log_message("DEBUG", "Reception aspose");
            }
            $file = array(
                "tmp_name" => $this->uploaded_path . $guid . ".pdf",
                "size" => filesize($this->uploaded_path . $guid . ".pdf"),
                "name" => $req->getNom_fichier() . ".pdf"
            );

            $type = $req->getType();
            $types_aspose_cr_reunion = array(4,8,14,18);
            if (in_array($type, $types_aspose_cr_reunion)) {
                $type = 109; // (type commun CR reunion)
            }
            $efile = $this->ci->files->move($file, $type, $req->getStatut(), $req->getSinistre()->getAffaire());

            if ($edition) {
                $oldoc = $edition->getDocument();
                if ($oldoc instanceof Entities\File) {
                    unlink($oldoc->getUri());
                }
                $edition->setDocument($efile);
                if ($req->getPreview() == true)
                    $edition->setEtat(1);
                else {
                    $edition->setEtat(2);
                    $doc = $edition->getDocument();
                    $doc->setSinistre($sinistre);
                    $this->ci->doctrine->em->persist($doc);
                }

                $this->ci->doctrine->em->persist($edition);
                $this->ci->doctrine->em->flush();

                if ($type == 109 && $edition->getEtat() == 2 && $edition->getDiffusion() != null) {
                    //$this->diffuser($edition->getId());
                } else if ($edition->getEtat() == 2) {
                    $statut = $edition->getStatut();
                    $data = json_decode($edition->getData());
                    $data_hist = array(
                        "cc" => null,
                        "to" => $edition->getDiffusion(),
                        "mail" => null,
                        "expediteur" => $edition->getAuteur(),
                        "objet" => $edition->getNom_tmp(),
                        "attachements" => array($edition->getDocument()->getId()));
                    $libelle = $edition->getNom_tmp();
                    $this->historiser($data_hist, $libelle, $statut, 2);
                }
            }
            $this->ci->doctrine->em->remove($req);
            $this->ci->doctrine->em->flush();
            if (file_exists($file['tmp_name']))
                unlink($file['tmp_name']);
        }
        $r = ob_get_clean();
        log_message("DEBUG", "Reception aspose : =====================================================================");
        log_message("DEBUG", $r);
        log_message("DEBUG", "========================================================================================");
        die($r);
    }

    public function diffuser($id) {
        $editions_repo = $this->ci->doctrine->em->getRepository('Entities\Edition');
        $edition = $editions_repo->find($id);

        $this->ci->load->helper('mailer');

        $from = $edition->getMail_auteur();
        $to = $edition->getDiffusion();
        $objet = "Diffusion de document : " . $edition->getDocument()->getNom();
        $pj = $edition->getDocument();

        $data = $edition->getData();
        $data = json_decode($data);
        $date = $data->date_reunion;
        $redacteur = $data->redacteur;

        $view = $this->ci->twig->render('mails/patrons/header.html.twig');
        $view .= $this->ci->twig->render('mails/patrons/cr_reunion.html.twig', array('date' => $date, 'from' => $redacteur));
        $view .= $this->ci->twig->render('mails/patrons/footer.html.twig');

        sendMail($view, $objet, $to, $from, null, $pj);

        $edition->setEtat(3);

        $statut = $edition->getStatut();

        $data_hist = array(
            "cc" => null,
            "to" => $to,
            "mail" => $view,
            "expediteur" => $edition->getAuteur(),
            "objet" => $objet,
            "attachements" => array($edition->getDocument()->getId()));

        $this->historiser($data_hist, $objet, $statut, 2);

        $this->ci->doctrine->em->persist($edition);
        $this->ci->doctrine->em->flush();
    }

    private function historiser($data, $libelle, $statut, $type) {
        $sinistre = $statut->getSinistre();
        $chrono = $this->ci->doctrine->em->getRepository('Entities\Historique')->getNextChrono($type, $sinistre);

        $hist = new \Entities\Historique();
        $hist->setDate(new DateTime);
        $hist->setLibelle($libelle);
        $hist->setNum_chrono($chrono);
        $hist->setSinistre($sinistre);
        $hist->setStatut($statut);
        $hist->setType($type);
        $hist->setData($data);

        $this->ci->doctrine->em->persist($hist);
        $this->ci->doctrine->em->flush();
    }

}
