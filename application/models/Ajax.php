<?php

class Ajax extends CI_Model {

    private $qualiperf;
    private $gaia;
    private $contentieux;

    public function __construct() {
        parent::__construct();
        $this->qualiperf = $this->load->database('qualiperf', true);
        $this->contentieux = $this->load->database('contentieux', true);
    }

    public function getCa() {
        $query = <<<SQL
                SELECT nom, prenom, id_utilisateur FROM utilisateurs WHERE date_sortie is null ORDER BY prenom + nom
SQL;

        $results = $this->qualiperf->query($query)->result();

        return $results;
    }

    public function getUsers() {
        $query = "SELECT nom, prenom, id_utilisateur FROM utilisateurs WHERE date_sortie is NULL ORDER BY prenom + nom";
        $results = $this->qualiperf->query($query)->result();

        return $results;
    }

    public function getCodes($term) {
        $query = "SELECT id as pk, code + ' ' + libelle as label, code as id from affaire where code like '%$term%' OR libelle like '%$term%'";
        $results = $this->contentieux->query($query)->result_array();

        return $results;
    }

    public function getRCR() {
        $results = array(993, 5785, 5695, 8920, 1053, 3950, 2769, 8899);

        $ids = "";
        foreach ($results as $result) {
            $ids .= "$result";
            if ($result != end($results)) {
                $ids .= ",";
            }
        }

        $query = "SELECT nom, prenom, email, id_utilisateur FROM utilisateurs where id_utilisateur IN ($ids)";
        $results = $this->qualiperf->query($query)->result();

        return $results;
    }

    public function getServices() {
        $query = "SELECT id_service, nom from services";
        $results = $this->qualiperf->query($query)->result();

        return $results;
    }

    // NEW
    //Autocomplete

    public function get_utilisateurs_qperf_autocomp($term) {
        $query = "SELECT prenom + ' ' + nom as label, id_utilisateur as id from utilisateurs WHERE (prenom + ' ' + nom LIKE '$term%' or nom + ' ' + prenom like '$term%') AND (date_sortie IS NULL OR date_sortie > getDate())";
        $r = $this->qualiperf->query($query)->result_array();
        return $r;
    }

    public function get_sv_qperf_autocomp($term) {
        $query = "SELECT id_service as id, nom as label from services where nom like '$term%' and date_cloture is null";
        $r = $this->qualiperf->query($query)->result_array();
        return $r;
    }

}
