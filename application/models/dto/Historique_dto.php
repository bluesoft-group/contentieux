<?php

class Historique_dto {

    public $id;
    public $sinistre_id;
    public $statut_id;
    public $type;
    public $date;
    public $data;
    public $num_chrono;
    public $libelle;
    public $type_mail;

}
