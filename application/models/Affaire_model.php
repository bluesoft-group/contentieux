<?php

class Affaire_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function get_row_affaire($id) {
        $query = "SELECT * FROM affaire WHERE id = ?";
        return $this->contentieux->query($query, array($id))->row();
    }
}
