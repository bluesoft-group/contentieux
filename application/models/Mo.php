<?php

class Mo extends Ci_Model {

    private $gaia;
    private $qualiperf;

    public function __construct() {
        parent::__construct();
        $this->gaia = $this->load->database('gaia', true);
        $this->qualiperf = $this->load->database('qualiperf', true);
    }

    public function getMo(&$affaire) {
        $mos = false;
        $mos_g = null;
        $mos_qp = null;

        if ($affaire->getId_g_affaire()) {
            $mos_g = $this->getMo_g($affaire);
        }
        if ($affaire->getId_qp_contrat()) {
            $mos_qp = $this->getMo_qp($affaire);
        }
        if (!$affaire->getId_g_affaire() && !$affaire->getId_qp_contrat() && $affaire->getNom_mo()) {
            $mos = array();
            $mos['mo']['type'] = 'Maître d\'ouvrage';
            $mos['mo']['adresse']['nom_societe'] = $affaire->getNom_mo();
            $mos['mo']['contacts'][0]['nom'] = $affaire->getMo();
        }

        if ($mos_g && $mos_qp) {
            $mos = array_merge($mos_g, $mos_qp);
        } else if ($mos_g) {
            $mos = $mos_g;
        } else if ($mos_qp) {
            $mos = $mos_qp;
        }

        return $mos;
    }

    /*
     * Récupère les Mo pour une affaire matchée ds gaia
     *
     * @return Array $mos
     * [
     * mo
     * [
     * mo.type
     * mo.adresse [
     *    -  mo.adresse.nom_societe
     *    -  mo.adresse.rue1
     *    -  mo.adresse.rue2
     *    -  mo.adresse.rue3
     *    -  mo.adresse.ville
     *    -  mo.adresse.code_postal
     * ]
     * mo.contacts [
     *    -  mo.contact.nom
     *    -  mo.contact.prenom
     *    -  mo.contact.telephone
     *    -  mo.contact.telephone_portable
     *    -  mo.contact.email
     * ]
     * ]
     * ]
     */

    public function getMo_g(Entities\Affaire &$affaire) {

        $id_affaire = $affaire->getId_g_affaire();

        $query = <<<SQL
                SELECT DISTINCT a.aff_code, a.aff_q_id_tiers_adresse, a.aff_q_id_tiers_adresse_bis, a.aff_id_mod_adresse, a.aff_id_amo_adresse, c.cmo_type, c.cmo_id_contact
                FROM GaiaGWT.dbo.gaia_affaire a
                INNER JOIN GaiaGWT.dbo.gaia_contact_mo c on c.cmo_id_affaire = a.id_affaire
                WHERE a.id_affaire = '$id_affaire'
SQL;
        $results = $this->gaia->query($query)->result();
        $mos = array();

        foreach ($results as $result) {

            if ($result->aff_q_id_tiers_adresse) {
                $mos[$result->aff_q_id_tiers_adresse]['type'] = "Maître d'ouvrage";
                $mos[$result->aff_q_id_tiers_adresse]['adresse'] = $this->getTiers($result->aff_q_id_tiers_adresse);
                $mos[$result->aff_q_id_tiers_adresse]['contacts'] = $this->getContactMo($id_affaire, 0);
                $affaire->setNom_mo($mos[$result->aff_q_id_tiers_adresse]['adresse']['nom_societe']);
                if ($affaire->getId()) {
                    $ci = &get_instance();
                    $ci->doctrine->em->persist($affaire);
                    $ci->doctrine->em->flush();
                }
            }
            if ($result->aff_q_id_tiers_adresse_bis) {
                $mos[$result->aff_q_id_tiers_adresse_bis]['type'] = "Maître d'ouvrage associé";
                $mos[$result->aff_q_id_tiers_adresse_bis]['adresse'] = $this->getTiers($result->aff_q_id_tiers_adresse_bis);
                $mos[$result->aff_q_id_tiers_adresse_bis]['contacts'] = $this->getContactMo($id_affaire, 1);
            }
            if ($result->aff_id_mod_adresse) {
                $mos[$result->aff_id_mod_adresse]['type'] = "Maître d'ouvrage délégué";
                $mos[$result->aff_id_mod_adresse]['adresse'] = $this->getTiers($result->aff_id_mod_adresse);
                $mos[$result->aff_id_mod_adresse]['contacts'] = $this->getContactMo($id_affaire, 2);
            }
            if ($result->aff_id_amo_adresse) {
                $mos[$result->aff_id_amo_adresse]['type'] = "Assistant Maître d'ouvrage";
                $mos[$result->aff_id_amo_adresse]['adresse'] = $this->getTiers($result->aff_id_amo_adresse);
                $mos[$result->aff_id_amo_adresse]['contacts'] = $this->getContactMo($id_affaire, 3);
            }
        }

        return $mos;
    }

    public function getTiers($id_tiers_adresse) {
        $query = <<<SQL
                SELECT t.nom as nom_societe,t.telephone as tel_societe, ta.adresse_1 as rue1, ta.adresse_2 as rue2, ta.adresse_3 as rue3, ta.code_postal, ta.ville
                FROM tiers_adresses ta
                INNER JOIN tiers t on t.id_tiers = ta.id_tiers
                WHERE ta.id_tiers_adresse = $id_tiers_adresse
SQL;

        $result = $this->qualiperf->query($query)->result_array();

        return $result[0];
    }

    public function getContactMo($id_affaire, $type) {
        $query = <<<SQL
                SELECT c.cmo_id_contact
                FROM gaia_contact_mo c
                INNER JOIN gaia_affaire a ON a.id_affaire = c.cmo_id_affaire
                WHERE a.id_affaire = '$id_affaire'
                AND cmo_type = $type
SQL;
        $result = $this->gaia->query($query)->result();
        $id_contact = @$result[0]->cmo_id_contact;
        if ($id_contact) {
            $query = <<<SQL
                SELECT nom, prenom, telephone, telephone_portable, email
                FROM contacts
                WHERE id_contact = $id_contact
SQL;
            $result = $this->qualiperf->query($query)->result_array();

            return $result;
        } else
            return null;
    }

    public function getMo_qp($affaire) {
        $id_contrat = $affaire->getId_qp_contrat();

        $query = <<<SQL
                SELECT c.libelle_avenant, t.nom as nom_societe, t.telephone as tel_societe,
                ta.adresse_1, ta.adresse_2, ta.adresse_3, ta.ville, ta.code_postal,
                co.nom, co.prenom, co.telephone, co.telephone_portable, co.email

                FROM contrats c
                INNER JOIN tiers t ON t.id_tiers = c.id_tiers
                INNER JOIN tiers_adresses ta ON ta.id_tiers_adresse = c.id_adresse_client
                LEFT JOIN contacts co ON co.id_contact = c.id_contact_interlocuteur_client

                WHERE id_contrat = $id_contrat

SQL;
        $result = $this->qualiperf->query($query)->result();

        if (isset($result[0])) {
            $mo = array();
            $mo['type'] = 'Maître d\'ouvrage';
            $mo['adresse'] = array(
                "nom_societe" => $result[0]->nom_societe,
                "tel_societe" => $result[0]->tel_societe,
                "rue1" => $result[0]->adresse_1,
                "rue2" => $result[0]->adresse_2,
                "rue3" => $result[0]->adresse_3,
                "ville" => $result[0]->ville,
                "code_postal" => $result[0]->code_postal
            );
            $mo['contacts'] = array(
                'nom' => $result[0]->nom,
                'prenom' => $result[0]->prenom,
                'telephone' => $result[0]->telephone,
                'telephone_portable' => $result[0]->telephone_portable,
                'email' => $result[0]->email
            );

            $affaire->setNom_mo($mo['adresse']['nom_societe']);
            if ($affaire->getId()) {
                $ci = &get_instance();
                $ci->doctrine->em->persist($affaire);
                $ci->doctrine->em->flush();
            }

            return array($mo);
        } else
            return false;
    }

}
