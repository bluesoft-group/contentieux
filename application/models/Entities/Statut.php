<?php

namespace Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="type", type="integer")
 * @DiscriminatorMap({1 = "Statut_DO", 2 = "Statut_AV1", 3 = "Statut_judiciaire", 4 = "Statut_amiable", 5 = "Statut_rp"})
 * @Table(name="statut")
 */
abstract class Statut {

    public function __construct() {
        $this->files = new ArrayCollection;
        $this->memos = new ArrayCollection;
    }

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @OneToMany(targetEntity="Entities\File", mappedBy="statut", cascade={"persist"}, orphanRemoval=true)
     * @OrderBy({"upload_date" = "DESC"})
     */
    private $documents;

    /**
     * @OneToMany(targetEntity="Entities\Memo", mappedBy="statut", cascade={"persist"}, orphanRemoval=true)
     * @OrderBy({"date" = "DESC"})
     */
    private $memos;

    /**
     * @OneToMany(targetEntity="Entities\Historique", mappedBy="statut")
     */
    private $historique;

    function getId() {
        return $this->id;
    }

    function getFiles() {
        return $this->files;
    }

    function addFile($file) {
        $this->files->add($file);
        $file->setStatut($this);
    }

    function getMemos() {
        return $this->memos;
    }

    function addMemo($memo) {
        $this->memos->add($memo);
        $memo->setStatut($this);
    }

    function delMemo($memo) {
        $this->memos->removeElement($memo);
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

    function getHistorique() {
        return $this->historique;
    }

    function addHistorique($historique) {
        $this->historique[] = $historique;
    }

    function getDocuments() {
        return $this->documents;
    }

    function addDocument($documents) {
        $this->documents[] = $documents;
    }

}
