<?php

namespace Entities;

/**
 * @Entity(repositoryClass="AdresseRepository")
 * @Table(name="adresse")
 */
Class Adresse {

    public function __construct($rue1 = null, $rue2 = null, $rue3 = null, $cp = null, $commune = null) {
        if ($rue1)
            $this->rue1 = substr($rue1, 0, 150);
        if ($rue2)
            $this->rue2 = substr($rue2, 0, 150);
        if ($rue3)
            $this->rue3 = substr($rue3, 0, 150);
        if ($cp)
            $this->cp = substr($cp, 0, 32);
        if ($commune)
            $this->commune = substr($commune, 0, 150);
    }

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $rue1;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $rue2;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $rue3;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $cp;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $commune;

    function getRue1() {
        return $this->rue1;
    }

    function getRue2() {
        return $this->rue2;
    }

    function getRue3() {
        return $this->rue3;
    }

    function getCp() {
        return $this->cp;
    }

    function getCommune() {
        return $this->commune;
    }

    function setRue1($rue1) {
        if ($rue1 == "") {
            $this->rue1 = "";
        } elseif ($rue1)
            $this->rue1 = substr($rue1, 0, 150);
    }

    function setRue2($rue2) {
        if ($rue2 == "") {
            $this->rue2 = "";
        } elseif ($rue2)
            $this->rue2 = substr($rue2, 0, 150);
    }

    function setRue3($rue3) {
        if ($rue3 == "") {
            $this->rue3 = "";
        } elseif ($rue3)
            $this->rue3 = substr($rue3, 0, 150);
    }

    function setCp($cp) {
        if ($cp == "") {
            $this->cp = "";
        } elseif ($cp)
            $this->cp = substr($cp, 0, 32);
    }

    function setCommune($commune) {
        if ($commune == "") {
            $this->commune = "";
        } elseif ($commune)
            $this->commune = substr($commune, 0, 150);
    }

    function __toString() {
        $str = $this->rue1;
        if ($this->rue2)
            $str .= PHP_EOL . $this->rue2;
        if ($this->rue3)
            $str .= PHP_EOL . $this->rue3;
        $str .= PHP_EOL . $this->cp . ' ' . $this->commune;

        return $str;
    }

}
