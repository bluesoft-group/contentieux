<?php

namespace Entities;

/**
 * @Entity
 * @Table(name="mailinglist")
 */

class MailingList {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     */
    
    private $type;
    
    /**
     * @Column(type="string", length=150, unique=false, nullable=false)
     */
    
    private $libelle;
    
    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    
    private $destinataires;
    
    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $ccs;
    
    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function getDestinataires() {
        return $this->destinataires;
    }

    function getCcs() {
        return $this->ccs;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setDestinataires($destinataires) {
        $this->destinataires = $destinataires;
    }

    function setCcs($ccs) {
        $this->ccs = $ccs;
    }



}
