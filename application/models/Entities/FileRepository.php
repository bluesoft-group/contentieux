<?php

namespace Entities;

use Doctrine\ORM\EntityRepository;

/**
 * HistoriqueRepository
 */
class FileRepository extends EntityRepository {

    public function getRapports_expert($statut = null) {
        return $this->findBy(array("num_type" => 102, "sinistre" => $statut->getSinistre()));
    }

    public function getDocs($statut, $type = null, $gaia = false, $ctx = true, $recherche = null, $date = null) {

        $ci = &get_instance();
        $affaire = $ci->context->getAffaire();

        if ($ctx) {
            if ($statut instanceof \Entities\Statut) {
                $sinistre = $statut->getSinistre();

                $qb = $this->getEntityManager()->createQueryBuilder();

                $req = $qb->select('f')
                        ->from('Entities\File', 'f')
                        ->where($qb->expr()->eq('f.sinistre', '?1'))
                        ->andWhere($qb->expr()->eq('f.statut', '?2'));
                if ($categorie != null) {
                    $qb->andWhere($qb->expr()->eq('f.categorie', '?3'))
                            ->setParameter(3, $categorie);
                }
                if ($recherche != null) {
                    $qb->andWhere($qb->expr()->like('f.nom', '?5'))
                            ->setParameter(5, "%$recherche%");
                }
                if ($type != null) {
                    $qb->andWhere($qb->expr()->eq('f.type', '?4'))
                            ->setParameter(4, $type);
                }
                if ($date != null) {
                    $qb->andWhere($qb->expr()->eq('f.date', "?6"))
                            ->setParameter(6, $date);
                }
                $qb->setParameter(1, $sinistre)
                        ->setParameter(2, $statut)
                        ->orderBy("f.type", "ASC");
            } else if ($statut instanceof Sinistre) {
                $qb = $this->getEntityManager()->createQueryBuilder();

                $req = $qb->select('f')
                        ->from('Entities\File', 'f')
                        ->where($qb->expr()->eq('f.sinistre', '?1'))
                        ->orWhere($qb->expr()->eq('f.affaire', '?7'))
                        ->setParameter(7, $affaire);
                if ($recherche != null) {
                    $qb->andWhere($qb->expr()->like('f.nom', '?5'))
                            ->setParameter(5, "%$recherche%");
                }
                if ($type != null) {
                    $qb->andWhere($qb->expr()->eq('f.type', '?4'))
                            ->setParameter(4, $type);
                }
                if ($date != null) {
                    $qb->andWhere($qb->expr()->eq('f.date', '?6'))
                            ->setParameter(6, $date, \Doctrine\DBAL\Types::DateTime);
                }
                $qb->setParameter(1, $statut)
                        ->orderBy("f.type", "ASC");
            }

            $result = $qb->getQuery()->getResult();
        } else {
            $result = array();
        }
        $ci = &get_instance();
        $ci->load->model('ImportDocuments');
        $aff = $ci->context->getAffaire();
        if ($gaia) {
            $docsGaia = $ci->ImportDocuments->getDocs($aff, "all");
            if ($docsGaia) {
                foreach ($docsGaia as $doc) {
                    $type_g = $ci->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => $doc["rap_type"]));
                    $date_d = date_create_from_format('Y-m-d H:i:s', $doc["rap_date"]);
                    $file = new File();
                    $file->setExt("pdf");
                    $file->setNom($doc["rap_filename"]);
                    $file->setUpload_date($date_d);
                    $file->setDate($date_d);
                    $file->setType($type_g);
                    $file->setUri("http://gaia.qualigroup.net/gaia/DownloadRapport?get=" . $doc["rap_publicKey"]);
                    $file->setId("g" . $doc["id_rapport"]);
                    $test = false;
                    if ((!$type && !$recherche && !$date) || ((!$type || $type == $type_g) && (!$recherche || (strstr(strtolower($file->getNom()), strtolower($recherche)) != false)) && (!$date || (date_format($date, 'd-m-Y') == date_format($date_d, 'd-m-Y')))))
                        $result[] = $file;
                }
            }
        }
        $contratsQp = $ci->ImportDocuments->getContratQp($aff);
        if ($contratsQp) {
            foreach ($contratsQp as $doc) {
                $file = new File();
                $nom_f_split = explode(".", $doc->nom_fichier);
                $type_q = $ci->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => 100));
                $file->setExt(end($nom_f_split));
                $file->setNom($doc->nom_fichier);
                $file->setUpload_date($doc->date_creation);
                $file->setType($type_q);
                $file->setUri("http://qualiperf.qualigroup.net/data/affaires/" . $doc->id_objet . "/" . $doc->nom_fichier);
                $file->setId("q" . $doc->id_document);
                $file->setDate($doc->date_creation);

                if ((!$type && !$recherche) || ((!$type || $type == $type_q) && (!$recherche || (strstr(strtolower($file->getNom()), strtolower($recherche)) != false)) && (!$date || (date_format($date, 'd-m-Y') == date_format($date_d, 'd-m-Y')))))
                    $result[] = $file;
            }
        }



        return $result;
    }

    public function getContrat($sinistre) {
        $affaire = $sinistre->getAffaire();
        $qb = $this->getEntityManager()->createQueryBuilder();

        $type_contrat = $this->getEntityManager()->getRepository('Entities\Type_docs')->findOneBy(array('num' => 100));
        $cat_contrat = $this->getEntityManager()->getRepository('Entities\Categories_docs')->findOneBy(array('num' => 6));

        $req = $qb->select('f')
                ->from('Entities\File', 'f')
                ->where($qb->expr()->eq('f.sinistre', '?1'))
                ->andWhere($qb->expr()->eq('f.type', '?2'))
                ->setParameter(1, $sinistre)
                ->setParameter(2, $type_contrat);

        $result = $qb->getQuery()->getResult();

        $ci = &get_instance();
        $ci->load->model('ImportDocuments');
        $contratsQp = $ci->ImportDocuments->getContratQp($affaire);
        if ($contratsQp) {
            foreach ($contratsQp as $doc) {
                $file = new File();
                $nom_f_split = explode(".", $doc->nom_fichier);
                $file->setExt(end($nom_f_split));
                $file->setNom($doc->nom_fichier);
                $file->setUpload_date($doc->date_creation);
                $file->setType($type_contrat);
                $file->setUri("http://qualiperf.qualigroup.net/data/affaires/" . $doc->id_objet . "/" . $doc->nom_fichier);
                $file->setId("q" . $doc->id_document);
                $file->setCategorie($cat_contrat);

                $result[] = $file;
            }
        }
        return $result;
    }

}
