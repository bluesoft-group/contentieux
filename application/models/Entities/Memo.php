<?php

namespace Entities;

/**
 * @Entity
 * @Table(name="memo")
 */
class Memo {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="datetime", unique=false, nullable=false)
     */
    private $date;

    /**
     * @Column(type="string", length=32, unique=false, nullable=false)
     */
    private $auteur;

    /**
     * @Column(type="string", length=500, unique=false, nullable=false)
     */
    private $memo;

    /**
     * @ManyToOne(targetEntity="Entities\Statut", cascade={"all"}, fetch="LAZY", inversedBy="memos")
     */
    private $statut;

    /**
     * @ManyToOne(targetEntity="Entities\Sinistre", cascade={"all"}, fetch="LAZY", inversedBy="memos")
     */
    private $sinistre;

    function getId() {
        return $this->id;
    }

    function getDate() {
        return $this->date;
    }

    function getAuteur() {
        return $this->auteur;
    }

    function getMemo() {
        return $this->memo;
    }

    function getStatut() {
        return $this->statut;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setAuteur($auteur) {
        $this->auteur = $auteur;
    }

    function setMemo($memo) {
        $this->memo = $memo;
    }

    function setStatut($statut) {
        $this->statut = $statut;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }
    
    function delete() {
        if ($this->sinistre){
            $this->getSinistre()->delMemo($this);
            $this->setSinistre (null);
        }
        if ($this->statut){
            $this->getStatut()->delMemo($this);
            $this->setStatut (null);
        }
    }
}
