<?php

class Roles_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function autocomplete_agence($term) {
        $query = "SELECT id_Agence, Agence FROM vue_UO_Service WHERE Agence like '$term%'";
    }

    public function get_roles() {
        $query = "SELECT * FROM rcr WHERE cp_agence = 1 ORDER BY nom_agence";
        $roles = $this->contentieux->query($query)->result_array();
        return $roles;
    }

    public function get_liste_agences() {
        $query = "SELECT DISTINCT Agence, id_Agence FROM vue_UO_Service 
                  WHERE id_Agence IS NOT null 
                  AND flag_centre_profit = 1
                  AND id_societe = 1
                  AND id_pole = 307
                  AND flag_apport_affaire = 1
                  ORDER BY Agence";

        return $this->qualiperf->query($query)->result_array();
    }

    public function remplir_roles() {
        $liste_agences = $this->get_liste_agences();
        foreach ($liste_agences as $agence) {
            $query = "INSERT INTO rcr (id_agence, nom_agence, cp_agence) VALUES (?,?,1)";
            $this->contentieux->query($query, array($agence["id_Agence"], $agence["Agence"]));
        }
    }

    public function appliquer_anciens_roles() {
        $query_old = "SELECT * FROM rcr WHERE cp_agence != 1";
        $anciens_roles = $this->contentieux->query($query_old)->result_array();

        foreach ($anciens_roles as $role) {
            $query = "UPDATE rcr SET id_rcr = ?, nom_rcr = ?, mail_rcr = ?, id_rca = ?, nom_rca = ?, mail_rca = ?, id_aca = ?, nom_aca = ?, mail_aca = ? WHERE id_agence = ? AND cp_agence = 1";
            $this->contentieux->query($query, array(
                $role["id_rcr"],
                $role["nom_rcr"],
                $role["mail_rcr"],
                $role["id_rca"],
                $role["nom_rca"],
                $role["mail_rca"],
                $role["id_aca"],
                $role["nom_aca"],
                $role["mail_aca"],
                $role["id_agence"]
            ));
        }
    }
    
    public function maj_affaires() {
        $query = "SELECT id, id_service_contentieux,nom_service_contentieux,  id_agence, code FROM affaire";
        $liste_affaires = $this->contentieux->query($query)->result_array();
        
        foreach ($liste_affaires as $affaire) {
            //test si l'id_agence de l'affaire existe
            $q_test_agence = "SELECT count(*) as cnt FROM rcr WHERE id_agence = ? AND cp_agence = 1";
            $r_test_agence = $this->contentieux->query($q_test_agence, array($affaire["id_agence"]))->row()->cnt;
            if ($r_test_agence > 0) {
                continue;
            } else {
                echo $affaire["code"] . " - id agence :" .$affaire["id_agence"] . " - " . $affaire["nom_service_contentieux"] . " (" .$affaire["id_service_contentieux"]. ")<br>";
                $agence = $this->get_agence_for_service($affaire["id_aservice_contentieux]"]);
            }
        }
    }
    
    public function get_agence_for_service($id_service) {
        $query = "SELECT id_Agence, Agence FROM vue_UO_Service WHERE id_service_QP = ?";
        return $this->qualiperf->query($query, array($id_service))->row_array();
    }

    public function get_email_utilisateur($id_utilisateur_qperf) {
        return $this->qualiperf->query("select email from utilisateurs where id_utilisateur = ?", array($id_utilisateur_qperf))->row()->email;
    }

    public function maj_role($type, $id_agence, $fullname, $id_utilisateur) {
        $email = $this->get_email_utilisateur($id_utilisateur);
        $query = "UPDATE rcr SET ";
        switch ($type) {
            case "rcr":
                $query .= "id_rcr = ?, nom_rcr = ?, mail_rcr = ? ";
                break;
            case "rca":
                $query .= "id_rca = ?, nom_rca = ?, mail_rca = ? ";
                break;
            case "aca":
                $query .= "id_aca = ?; nom_aca = ?, mail_aca = ? ";
                break;
        }

        $query .= "WHERE id_agence = ? AND cp_agence = 1";

        $this->contentieux->query($query, array($id_utilisateur, $fullname, $email, $id_agence));
    }

}
