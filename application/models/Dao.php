<?php

function autoload_model($class) {
    if (file_exists(APPPATH . "models/$class.php")) {
        include_once APPPATH . "models/$class.php";
    } elseif (file_exists(APPPATH . "models/dto/$class.php")) {
        include_once APPPATH . "models/dto/$class.php";
    }
}
spl_autoload_register("autoload_model");

class Dao extends CI_Model {

    protected $contentieux;
    protected $qualiperf;
    protected $gaia;

    public function __construct() {
        parent::__construct();

        $this->contentieux = $this->load->database("contentieux", true);
        $this->qualiperf = $this->load->database("qualiperf", true);
        $this->gaia = $this->load->database("gaia", true);


        $this->contentieux->query("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
        $this->gaia->query("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
        $this->qualiperf->query("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
    }

    protected function mapDatas($datas, $classname) {
        $instance = new $classname();

        foreach ($instance as $key => $value)
            $instance->$key = @$datas[$key];

        return $instance;
    }

}
