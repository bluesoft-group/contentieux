$(document).ready(function() {

    dropdown_choix_multiple = (function() {

        let ddcm = {};

        let styles = `
        <style>
            .ddcm-selected ul {
                margin-left: 10px;
                padding-left: 0px;
                padding-top: 3px;

                border-left: 1px solid #ccc;
                -webkit-border-image:
                -webkit-gradient(linear, 0 0, 0 100%, from(#ccc), to(rgba(0, 0, 0, 0))) 1 100%;
                padding-bottom: 2px;
            }

            .ddcm-li-selected:before {
                content: ' ✓ ';
                padding-left: 5px;
                color: green;
                font-weight: bold;
            }

            .ddcm-li-selected {
                /*border-left: 1px solid #ccc;*/
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
            }
        </style>
        `;
        $("HEAD").append(styles);

        $("select[type='ddcm']").toArray().forEach(function(elem) {
            $(elem).hide();

            let name = $(elem).attr("name");

            let checkboxes = document.createElement("DIV");
            let li_selected = "";

            $(elem).find("option").toArray().forEach(function(opt) {

                let libelle = $(opt).contents()[0].data;
                let value = $(opt).attr("value");

                let checkbox = document.createElement("input");
                checkbox.type = "checkbox";
                checkbox.value = value;
                checkbox.className = "ddcm";
                checkbox.style.marginRight = "10px";

                if (opt.selected) {
                    checkbox.setAttribute("checked", "checked");
                    li_selected += `<li class='ddcm-li-selected' value='${value}'>${libelle}</li>`;
                } else {
                    li_selected += `<li style='display: none' class='ddcm-li-selected' value='${value}'>${libelle}</li>`;
                }
                if ($(elem).attr("ddcm-display-selected") == "true") {
                    checkbox.setAttribute("display-selected", "true");
                }

                for (let attr in opt.attributes) {
                    let elem = opt.attributes[attr];
                    if (elem.name !== "selected") {
                        $(checkbox).attr(elem.name, elem.nodeValue);
                    }
                }

                let label = document.createElement("label");
                let li = document.createElement("li");

                let txt = document.createTextNode(libelle);

                label.appendChild(checkbox);
                label.appendChild(txt);
                li.appendChild(label);

                checkboxes.appendChild(li);

                test = opt;
            });

            let styles_dd = "";
            let dd_width = "";
            if ($(elem).attr("ddcm-style")) {
                styles_dd = 'style="'+$(elem).attr("ddcm-style")+'"';
            }
            if ($(elem).attr("ddcm-width")) {
                dd_width = "width:" + $(elem).attr("ddcm-width");
            }

            let dd = `<div ${styles_dd}>
            <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="width: 100%; display: flex; justify-content: space-between; align-items: center">
            ${ name } <span class="caret pull-right" style="top: 10px"></span>
            </button>
            <ul class="dropdown-menu ul-ddcm" style='padding: 5px 10px; max-height: 300px !important; overflow-y: auto; ${dd_width}'>
            ${ checkboxes.innerHTML }
            </ul>
            </div>`;

            if ($(elem).attr("ddcm-display-selected") == "true") {
                dd += `<div class='ddcm-selected' style><ul>${li_selected}</ul></div>`;
            }

            dd += "</div>";


            $(dd).insertAfter(elem);
        });

        $(document).on("change", "input[type='checkbox'].ddcm", function() {

            let select = $(this).closest("div.dropdown").parent()[0].previousSibling;
            let value = $(this).attr("value");
            if ($(this).is(":checked")) {
                $(select).find("option[value='"+value+"']")[0].selected = true;
            } else {
                $(select).find("option[value='"+value+"']")[0].selected = false;
            }
            $(select).change();

            if ($(this).attr("display-selected") == "true") {
                let div_preview = $(this).closest(".dropdown")[0].nextSibling;
                if ($(this).is(":checked")) {
                    $(div_preview).find("li[value='"+value+"']").show();
                } else {
                    $(div_preview).find("li[value='"+value+"']").hide();
                }
            }

        });

        $(document).on('click', 'ul.ul-ddcm', function(event){
            event.stopPropagation();
        });

        return ddcm;
    }());
});
